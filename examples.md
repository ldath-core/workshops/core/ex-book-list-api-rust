# graphQL
## get
### book
```

query book {
  book(id: 1) {
    id
    title
    description
    author
    image {
      type
      data
    }
    year
  }
}
```
### books
```
query books {
    books(skip: 0 limit: 120) {
    id
    title
    description
    author
    image {
      type
      data
    }
    year
  }
}
```

## health
```
query health {
  health {
    alive
    db
  }
}
```
## create
```
mutation create {
  createBook(book: {
    title: "title"
    author: "author"
    year: "2137"
    description: "description"
    image: {
      type: BASE_64
      data: "==base64"
    }
  }) {
    id
    title
    author
    year
    description
    image {
      type
      data
    }
  }
}
```

## update
```
TODO
```

## delete
```
mutation delete {
  deleteBook (id: 2 )
}
```

## book_events subscription
```
subscription eventbus {
  bookEvents {
    eventType
    book {
      id
      title
      author
      description
      image {
        type
        data
      }
    }
    id
    }
}
```

# REST
## get
### book
### books
## health
## create
## update
## delete