#!/usr/bin/env bash
./target/release/book-list --config ./secrets/local.env.yaml --logger-format full serve -b 127.0.0.1 -p 8882 --migrate --load --cors
