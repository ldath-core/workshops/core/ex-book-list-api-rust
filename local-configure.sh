#!/usr/bin/env bash
set -e
ansible-playbook --vault-id @prompt -i 'localhost,' --connection=local local-playbook.yml
