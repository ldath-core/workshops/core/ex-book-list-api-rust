#!/usr/bin/env sh
set -e
cargo test --no-run
cargo test -- --test-threads 1
