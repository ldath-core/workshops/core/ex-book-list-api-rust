#!/usr/bin/env bash
flamegraph -o serve-perf.svg -- ./target/release/book-list --config ./secrets/local.env.yaml --logger-format compact serve -b 127.0.0.1 -p 8882 --migrate --load --cors
