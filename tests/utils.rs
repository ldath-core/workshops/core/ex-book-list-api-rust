use assert_fs::{prelude::*, NamedTempFile};
use axum::body::HttpBody;
use http::Uri;
use hyper::{Body, Response};
use rand::{self, Rng};
use serde_json::Value;
use sqlx::{query, PgPool};
use std::{
    io::{BufRead, BufReader},
    net::{SocketAddr, TcpListener},
    process::{ChildStdout, Command},
    time::{Duration, Instant},
};

const REDIS_URL: &str = "redis://:devRedisPassword@127.0.0.1:6379";

#[must_use]
pub fn get_tmp_cfg(db_str: &str, log_level: &str) -> NamedTempFile {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(&format!(
            r#"---
env: dev
server:
  data:
postgres:
  url: {db_str}
redis:
  url: {REDIS_URL}
  key_expiry_seconds: 0
logger:
  level: {log_level}
"#
        ))
        .unwrap();
    tmp_cfg
}

pub fn read_stdout_until_contains(
    bufread: &mut BufReader<ChildStdout>,
    pat: &str,
    timeout: u64,
) -> Option<()> {
    let start = Instant::now();
    let mut buf = String::new();
    while let Ok(n) = bufread.read_line(&mut buf) {
        let elapsed = start.elapsed();
        if elapsed > Duration::from_secs(timeout) {
            return None;
        }
        if n > 0 {
            tracing::info!("{buf}");
            if buf.contains(pat) {
                return Some(());
            }
            buf.clear();
        }
    }
    None
}

/// Retries the request continously for n seconds
pub async fn retry_request(uri: Uri, n: u32) -> Option<Response<Body>> {
    for _ in 0..n * 2 {
        let response = hyper::Client::new().get(uri.clone()).await;
        if let Ok(resp) = response {
            return Some(resp);
        }
        tokio::time::sleep(std::time::Duration::from_millis(500)).await;
    }
    None
}

pub fn send_sigint(pid: u32) {
    Command::new("kill")
        .args(["-SIGINT", &pid.to_string()])
        .spawn()
        .unwrap();
}

pub async fn create_random_empty_database(pool: &PgPool) -> String {
    const CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz";
    const DB_NAME_LEN: usize = 30;
    let mut rng = rand::thread_rng();
    let new_database: String = (0..DB_NAME_LEN)
        .map(|_| {
            let idx = rng.gen_range(0..CHARSET.len());
            CHARSET[idx] as char
        })
        .collect();

    query(&format!("CREATE DATABASE {new_database}"))
        .execute(pool)
        .await
        .unwrap();
    new_database
}

pub async fn drop_database(name: &str, pool: &PgPool) {
    query(&format!("DROP DATABASE {name}"))
        .execute(pool)
        .await
        .unwrap();
}

/// Starts a listener on a random port
/// Returns listener instance and corresponding socket address
#[must_use]
pub fn get_random_port_listener(ip: &str) -> (TcpListener, SocketAddr) {
    let listener = TcpListener::bind((ip, 0)).unwrap();
    let addr = listener.local_addr().unwrap();
    (listener, addr)
}

pub async fn body_as_json<T>(r: hyper::Response<T>) -> Value
where
    T: HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();
    let body: Value = serde_json::from_slice(&hyper::body::to_bytes(into).await.unwrap()).unwrap();
    body
}

#[must_use]
pub fn uri(addr: SocketAddr, path_and_query: &str) -> Uri {
    let mut parts = http::uri::Parts::default();
    parts.scheme = Some("http".parse().unwrap());
    parts.authority = Some(addr.to_string().parse().unwrap());
    parts.path_and_query = Some(path_and_query.parse().unwrap());
    Uri::from_parts(parts).unwrap()
}
