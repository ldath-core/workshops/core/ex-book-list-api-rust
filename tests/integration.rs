use assert_cmd::prelude::*;
use http::{Request, Response, StatusCode, Uri};
use hyper::Body;
use once_cell::sync::OnceCell;
use predicates::prelude::*;
use serde_json::{json, Value};
use sqlx::PgPool;
use std::io::BufReader;
use std::net::SocketAddr;
use std::process::{Child, Command, Stdio};
use utils::get_tmp_cfg;

mod utils;
use utils::{
    body_as_json, create_random_empty_database, drop_database, get_random_port_listener,
    read_stdout_until_contains, retry_request, send_sigint, uri,
};

const ADMIN_CONN_STR: &str = "postgres://book-list:2vQU45haQCnDS8sO@localhost:5432/book-list";
const LISTEN_IP: &str = "127.0.0.1";

#[allow(clippy::redundant_closure)]
static ADMIN_PG_POOL: OnceCell<PgPool> = OnceCell::new();

#[ctor::ctor]
async fn setup() {
    tokio::runtime::Runtime::new().unwrap().block_on(async {
        let admin_pool = sqlx::PgPool::connect(ADMIN_CONN_STR).await.unwrap();
        ADMIN_PG_POOL.get_or_init(|| admin_pool);
    });

    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())
        .expect("setting tracing default failed");
}

#[test]
fn help_command_succeeds() {
    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args(["serve", "--help"]);
    cmd.assert().success();
}

#[test]
fn nonexistent_subcommand_fails() {
    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args(["this-subcommand-does-not-exist"]);
    cmd.assert().failure();
}

#[test]
fn default_log_level_from_config() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args(["--config", cfg.path().to_str().unwrap(), "serve"]);

    cmd.assert().stdout(
        predicate::str::contains(r#""verbosity":"Level(Debug)""#)
            .and(predicate::str::contains(r#""format":"Json""#)),
    );
}

#[test]
fn overwrite_log_level_from_cli() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args(["--config", cfg.path().to_str().unwrap(), "-vv", "serve"]);

    cmd.assert()
        .stdout(predicate::str::contains(r#""verbosity":"Level(Trace)""#));
}

#[tokio::test(flavor = "multi_thread")]
async fn server_starts_and_shutdowns_gracefuly() {
    let ctx = TestContext::new().await;
    let cfg = get_tmp_cfg(&get_conn_str(&ctx.db_name), "debug");

    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args([
        "--config",
        cfg.path().to_str().unwrap(),
        "serve",
        "--bind",
        LISTEN_IP,
        "--port",
        &ctx.addr.port().to_string(),
    ]);
    let mut running = cmd.stdout(Stdio::piped()).spawn().unwrap();
    let mut bufread = BufReader::new(running.stdout.take().unwrap());

    tracing::info!("{}", running.id());
    read_stdout_until_contains(&mut bufread, "Serve params", 15).unwrap();
    send_sigint(running.id());
    read_stdout_until_contains(&mut bufread, "starting graceful shutdown", 15).unwrap();
}

#[tokio::test(flavor = "multi_thread")]
async fn add_then_get_book() {
    let tested_book_json = get_new_book_json;

    let mut ctx = TestContext::new().await;
    let cfg = get_tmp_cfg(&get_conn_str(&ctx.db_name), "debug");

    let mut cmd = Command::cargo_bin("book-list").unwrap();
    cmd.args([
        "--config",
        cfg.path().to_str().unwrap(),
        "serve",
        "--bind",
        LISTEN_IP,
        "--port",
        &ctx.addr.port().to_string(),
        "--migrate",
        "--load",
    ]);
    let handle = cmd.spawn().unwrap();
    ctx.spawned = Some(handle);

    retry_request(uri(ctx.addr, "/v1/health"), 15)
        .await
        .unwrap();

    let resp = send_req(
        Request::post(uri(ctx.addr, "/v1/books"))
            .header("content-type", "application/json")
            .body(Body::from(serde_json::to_vec(&tested_book_json()).unwrap()))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::CREATED);
    let id = body_as_json(resp).await["content"]["id"].as_i64().unwrap() as i32;

    let resp = send_req(get(uri(ctx.addr, &format!("/v1/books/{id}")))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    for (k, v) in tested_book_json().as_object().unwrap() {
        assert_eq!(&body["content"][k], v);
    }
}

fn get_new_book_json() -> Value {
    json!({
        "title": "newTitle",
        "author": "newAuthor",
        "year": "1999",
        "description": "newDescription",
        "image": {
            "type": "url",
            "data": "site.com/image.png"
        }
    })
}

struct TestContext {
    db_name: String,
    spawned: Option<Child>,
    addr: SocketAddr,
}

impl TestContext {
    async fn new() -> Self {
        let (_listener, addr) = get_random_port_listener(LISTEN_IP);
        // willingly allow listener to drop out of scope, leaving us with a free port
        Self {
            db_name: create_random_empty_database(ADMIN_PG_POOL.get().unwrap()).await,
            spawned: None,
            addr,
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        if self.spawned.is_some() {
            self.spawned.as_mut().unwrap().kill().unwrap();
        }
        let h = match tokio::runtime::Handle::try_current() {
            Ok(h) => h,
            Err(_) => tokio::runtime::Runtime::new().unwrap().handle().clone(),
        };
        let db_name = self.db_name.clone();
        tokio::task::block_in_place(move || {
            h.block_on(drop_database(&db_name, ADMIN_PG_POOL.get().unwrap()));
        });
    }
}

#[must_use]
pub fn get_conn_str(db_name: &str) -> String {
    format!("postgres://book-list:2vQU45haQCnDS8sO@localhost:5432/{db_name}")
}

fn get(uri: Uri) -> Request<Body> {
    Request::get(uri).body(Body::empty()).unwrap()
}

async fn send_req(req: Request<Body>) -> Response<Body> {
    let client = hyper::Client::new();
    client.request(req).await.unwrap()
}
