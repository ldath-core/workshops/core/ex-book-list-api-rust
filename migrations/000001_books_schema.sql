DO $$ BEGIN
    CREATE TYPE image_type AS ENUM ('url', 'base64');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
    CREATE TYPE image AS (type image_type, data text);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS books (
    id SERIAL,
    title text NOT NULL,
    author text NOT NULL,
    year text NOT NULL,
    description text,
    image image
);
