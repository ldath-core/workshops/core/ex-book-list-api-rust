FROM rust:1.67 as builder

WORKDIR /var/app
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
RUN cargo build --release 

FROM debian:buster-slim

WORKDIR /var/app
RUN apt-get update && apt-get install libssl-dev -y
COPY --from=builder /var/app/target/release/book-list /bin/book-list
COPY migrations /var/app/migrations
RUN chmod +x /bin/book-list

EXPOSE 8080
ENTRYPOINT ["/bin/book-list"]
CMD ["--config", "/secrets/local.env.yaml", "-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
