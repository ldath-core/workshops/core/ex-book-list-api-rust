use borsh::{BorshDeserialize, BorshSerialize};
use serde::{Deserialize, Serialize};
use sqlx::{postgres::PgRow, FromRow, Row, Type};
use tracing::debug;

#[cfg(test)]
use crate::router::NewBook;
use crate::router::{NewImage, UpdateImage};

/// Object representing book in a way serializable to and from database
#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Serialize, BorshSerialize, BorshDeserialize, FromRow, Debug)]
pub struct Book {
    pub id: i32,
    pub title: String,
    pub author: String,
    pub year: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub image: Option<Image>,
}

/// Represents optional image data which is a model of postgres type
#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Serialize, BorshSerialize, BorshDeserialize, FromRow, Debug, Type)]
#[sqlx(type_name = "image")]
pub struct Image {
    #[serde(rename = "type")]
    pub type_: ImageType,
    pub data: String,
}

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Serialize, Deserialize, BorshDeserialize, BorshSerialize, Debug, Type)]
#[serde(rename_all = "lowercase")]
#[sqlx(type_name = "image_type", rename_all = "lowercase")]
pub enum ImageType {
    Base64,
    Url,
}

impl FromRow<'_, PgRow> for ImageType {
    fn from_row(row: &PgRow) -> sqlx::Result<Self> {
        let t: String = row.try_get("image.type")?;
        debug!("got {t} from row that should have image");
        match t.as_str() {
            "url" => Ok(Self::Url),
            "base64" => Ok(Self::Base64),
            _ => panic!(),
        }
    }
}

impl From<NewImage> for Image {
    fn from(item: NewImage) -> Self {
        Self {
            type_: item.type_,
            data: item.data,
        }
    }
}

impl From<UpdateImage> for Image {
    fn from(item: UpdateImage) -> Self {
        Self {
            type_: item.type_,
            data: item.data,
        }
    }
}

#[cfg(test)]
impl PartialEq<NewBook> for Book {
    fn eq(&self, other: &NewBook) -> bool {
        let image = other.image.as_ref().map(|i| Image::from(i.clone()));
        self.title == other.title
            && self.author == other.author
            && self.year == other.year
            && self.description == other.description
            && self.image == image
    }
}

#[cfg(test)]
impl PartialEq<NewImage> for Image {
    fn eq(&self, other: &NewImage) -> bool {
        self.type_ == other.type_ && self.data == other.data
    }
}
