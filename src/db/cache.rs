use anyhow::{anyhow, Result};
use borsh::{BorshDeserialize, BorshSerialize};
use deadpool_redis::{Config, Connection, Pool, Runtime};
use redis::AsyncCommands;
use std::{fmt::Debug, time::Duration};
use tracing::{debug, error, info, instrument, trace};

#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct Cache {
    pool: Pool,
    expiry: Duration,
}

impl Cache {
    pub async fn new(pool: Pool, expiry: Duration) -> Self {
        Self { pool, expiry }
    }

    #[instrument(skip(conn_str))]
    pub async fn connect_redis(conn_str: &str) -> Result<Pool> {
        debug!("redis connection string: {}", conn_str);
        let cfg = Config::from_url(conn_str);
        let pool = cfg.create_pool(Some(Runtime::Tokio1)).unwrap();
        info!("Connected to redis");
        Ok(pool)
    }

    #[instrument]
    pub async fn flush(&self) -> Result<()> {
        let mut con = self.get_con().await?;
        redis::cmd("FLUSHDB").query_async(&mut con).await?;
        Ok(())
    }

    async fn get_con(&self) -> Result<Connection> {
        self.pool.get().await.map_err(|e| {
            error!("couldn't get async redis connection with err {e}");
            e.into()
        })
    }

    #[instrument]
    pub async fn get<T: BorshDeserialize>(&self, key: &str) -> Result<T> {
        let mut con = self.get_con().await?;
        let out: Vec<u8> = con.get(key).await.map_err(|e| {
            error!("couldn't get key, {e}");
            e
        })?;
        if out.is_empty() {
            return Err(anyhow!(
                "key {} returned empty value, probably was unset",
                key
            ));
        }
        trace!("binary value returned from redis for {key} : {out:?}");
        let res = T::try_from_slice(&out).map_err(|e| {
            error!("Couldn't deserialize {key} retrieved from redis. Error: {e}");
            e
        })?;
        Ok(res)
    }

    #[instrument(skip(t))]
    pub async fn set<T: BorshSerialize>(&self, key: &str, t: &T) -> Result<()> {
        let mut con = self.get_con().await?;
        let v = t.try_to_vec().map_err(|e| {
            error!("Couldn't encode books for {key} as vec");
            e
        })?;
        con.set_ex(key, &v, self.expiry.as_secs() as usize)
            .await
            .map_err(|e| {
                error!("Couldn't save encoded {key} value to redis");
                e
            })?;
        Ok(())
    }

    #[instrument]
    pub async fn delete(&self, key: &str) -> Result<()> {
        let mut con = self.get_con().await?;
        con.del(key).await.map_err(|e| {
            error!("Couldn't delete cached {key} value from redis");
            e
        })?;
        Ok(())
    }
}

impl Debug for Cache {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Cache")
            .field("expiry", &self.expiry)
            .field("pool.is_closed()", &self.pool.is_closed())
            .finish()
    }
}
