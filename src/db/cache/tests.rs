//! reflection on the test name is problematic so `tested_key` in tests that use it
//! has test name manually written into the key. Make sure no `tested_keys` are conflicting

const REDIS_CONN_STR: &str = "redis://:devRedisPassword@127.0.0.1:6379";
const REDIS_EXPIRY_SEC: u64 = 60;

use super::*;
use crate::db::Book;

#[ctor::ctor]
fn suite_setup() {
    let r = tokio::runtime::Runtime::new().unwrap();
    r.block_on(async {
        let pool = Cache::connect_redis(REDIS_CONN_STR).await.unwrap();
        let cache = Cache::new(pool, Duration::from_secs(60)).await;
        cache.flush().await.unwrap();
    });
}

#[tokio::test]
async fn cache_can_be_initialized() {
    let pool = Cache::connect_redis(REDIS_CONN_STR).await.unwrap();
    let _ = Cache::new(pool, Duration::from_secs(60)).await;
}

#[tokio::test]
async fn set_vec_u8() {
    let cache = prepare_test().await;
    cache.set("key:set_vec_u8", &vec![0u8, 10]).await.unwrap();
}

#[tokio::test]
async fn set_str() {
    let cache = prepare_test().await;
    cache.set("key:set_str", &"test &str").await.unwrap();
}

#[tokio::test]
async fn set() {
    let cache = prepare_test().await;
    let tested_book = get_book;

    cache.set("key:set", &tested_book()).await.unwrap();
}

#[tokio::test]
async fn set_then_get_vec_u8() {
    let cache = prepare_test().await;
    let tested_key = "key:set_then_get_vec_u8";
    let tested_vec = vec![0u8; 10];

    cache.set(tested_key, &tested_vec).await.unwrap();
    let result: Vec<u8> = cache.get(tested_key).await.unwrap();
    assert_eq!(result, tested_vec);
}

#[tokio::test]
async fn set_then_get_str() {
    let cache = prepare_test().await;
    let tested_key = "key:set_then_get_str";
    let tested_str = "tested &str";

    cache.set(tested_key, &tested_str).await.unwrap();
    let result: String = cache.get(tested_key).await.unwrap();
    assert_eq!(tested_str, result.as_str());
}

#[tokio::test]
async fn set_then_get() {
    let cache = prepare_test().await;
    let tested_book = get_book;
    let tested_key = "key:set_then_get";

    cache.set(tested_key, &tested_book()).await.unwrap();
    let book: Book = cache.get(tested_key).await.unwrap();
    assert_eq!(book, tested_book());
}

#[tokio::test]
async fn set_multiple_then_flush() {
    let cache = prepare_test().await;
    let tested_key_base = "key:set_multiple_then_flush";
    let n_tested = 10;

    for i in 0..n_tested {
        let key = format!("{tested_key_base}:{i}");
        cache.set(&key, &"doesn't matter").await.unwrap();
    }
    cache.flush().await.unwrap();
    for i in 0..n_tested {
        let key = format!("{tested_key_base}:{i}");
        // <String> result type is important, <()> would pass
        let result = cache.get::<String>(&key).await;
        assert!(result.is_err());
    }
}

#[tokio::test]
async fn set_then_delete_then_try_get() {
    let cache = prepare_test().await;
    let tested_key = "key:set_then_delete_then_try_get";

    cache.set(tested_key, &"doesn't matter").await.unwrap();
    cache.delete(tested_key).await.unwrap();
    // <String> result type is important, <()> would pass
    let result = cache.get::<String>(tested_key).await;
    assert!(result.is_err());
}

#[tokio::test]
async fn try_to_delete_inexistent() {
    let cache = prepare_test().await;
    let tested_key = "key:try_to_delete_inexistent";

    let result = cache.delete(tested_key).await;
    assert!(result.is_ok());
}

#[tokio::test]
async fn try_to_get_inexistent() {
    let cache = prepare_test().await;
    let tested_key = "key:try_to_get_inexistent";
    // <String> result type is important, <()> would pass
    let result = cache.get::<String>(tested_key).await;
    assert!(result.is_err());
}

async fn prepare_test() -> Cache {
    let pool = Cache::connect_redis(REDIS_CONN_STR).await.unwrap();

    Cache::new(pool, Duration::from_secs(REDIS_EXPIRY_SEC)).await
}

fn get_book() -> Book {
    Book {
        id: -1,
        title: "book1".to_string(),
        author: "author1".to_string(),
        year: "2137".to_string(),
        description: Some("test with some field".to_string()),
        image: None,
    }
}
