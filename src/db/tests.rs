use std::time::Duration;

use once_cell::sync::OnceCell;

use super::*;
use crate::{
    router::{NewImage, UpdateImage},
    tests::utils::{create_random_database, drop_database, get_temporary_db_pool},
};

const BOOK_DB_TEMPLATE: &str = "books_test_template";
const INITIALIZE_SUBSCRIBER: bool = false;
const ADMIN_CONN_STR: &str = "postgres://book-list:2vQU45haQCnDS8sO@localhost:5432/book-list";
const REDIS_CONN_STR: &str = "redis://:devRedisPassword@127.0.0.1:6379";
const REDIS_EXPIRY_SEC: u64 = 0;

#[allow(clippy::redundant_closure)]
static ADMIN_PG_POOL: OnceCell<PgPool> = OnceCell::new();

#[ctor::ctor]
async fn setup() {
    if INITIALIZE_SUBSCRIBER {
        tracing_subscriber::fmt()
            .with_max_level(tracing::Level::TRACE)
            .init();
    };
    let r = tokio::runtime::Runtime::new().unwrap();
    r.block_on(async {
        let admin_pool = sqlx::PgPool::connect(ADMIN_CONN_STR).await.unwrap();
        ADMIN_PG_POOL.get_or_init(|| admin_pool);

        query(&format!(
            " UPDATE pg_database SET datistemplate='false' WHERE datname='{BOOK_DB_TEMPLATE}' "
        ))
        .execute(ADMIN_PG_POOL.get().unwrap())
        .await
        .unwrap();

        query(&format!("DROP DATABASE IF EXISTS {BOOK_DB_TEMPLATE};"))
            .execute(ADMIN_PG_POOL.get().unwrap())
            .await
            .unwrap();

        query(&format!(
            "CREATE DATABASE {BOOK_DB_TEMPLATE} WITH is_template TRUE;"
        ))
        .execute(ADMIN_PG_POOL.get().unwrap())
        .await
        .unwrap();

        let template_pool = get_temporary_db_pool(BOOK_DB_TEMPLATE).await;
        let cache = Cache::new(
            Cache::connect_redis(REDIS_CONN_STR).await.unwrap(),
            Duration::from_secs(REDIS_EXPIRY_SEC),
        )
        .await;
        let event_bus = Arc::new(EventBus::new());
        let db = Db::new(template_pool, cache, event_bus).await.unwrap();
        db.migrate_db().await.unwrap();
        db.create_test_data().await.unwrap();
    });
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_books() {
    let ctx = TestContext::new().await;

    let result = ctx.db.get_books(0, 10).await;
    assert!(matches!(result, Ok(_)));
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_get_book() {
    let tested_book = new_book;

    let ctx = TestContext::new().await;

    let created = ctx.db.create_book(tested_book()).await.unwrap();

    let queried = ctx.db.get_book(created.id).await.unwrap();
    assert_eq!(created, queried);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_remove_book() {
    let tested_book = new_book;

    let ctx = TestContext::new().await;

    let created = ctx.db.create_book(tested_book()).await.unwrap();

    let deleted = ctx.db.delete_book(created.id).await.unwrap();
    assert_eq!(deleted, 1);

    let queried = ctx.db.get_book(created.id).await;
    assert!(matches!(queried, Err(_)));
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_update_book_all_fields() {
    let tested_new_book = new_book;
    let tested_update_book = update_book_all_fields;

    let ctx = TestContext::new().await;

    let created = ctx.db.create_book(tested_new_book()).await.unwrap();

    let updated = ctx
        .db
        .update_book(created.id, tested_update_book())
        .await
        .unwrap();
    assert_ne!(created, updated);

    let queried = ctx.db.get_book(created.id).await.unwrap();
    assert_eq!(queried, updated);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_update_title_only() {
    let tested_new_book = new_book;
    let tested_update_book = update_book_title_only;

    let ctx = TestContext::new().await;

    let created = ctx.db.create_book(tested_new_book()).await.unwrap();

    let updated = ctx
        .db
        .update_book(created.id, tested_update_book())
        .await
        .unwrap();
    assert_ne!(created, updated);
    assert_eq!(updated.title, tested_update_book().title.unwrap());

    let queried = ctx.db.get_book(created.id).await.unwrap();
    assert_eq!(queried, updated);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_update_image_base64_only() {
    let tested_new_book = new_book;
    let tested_update_book = update_book_image_base64_only;

    let ctx = TestContext::new().await;

    let created = ctx.db.create_book(tested_new_book()).await.unwrap();

    let updated = ctx
        .db
        .update_book(created.id, tested_update_book())
        .await
        .unwrap();
    assert_ne!(created, updated);
    assert_eq!(updated.image, tested_update_book().image.map(Image::from));

    let queried = ctx.db.get_book(created.id).await.unwrap();
    assert_eq!(queried, updated);
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_then_retrieve_many() {
    let tested_books = new_books;

    let ctx = TestContext::new().await;
    // given DB is empty
    ctx.db.migrate_db().await.unwrap();
    // when we insert multiple new books
    for b in tested_books() {
        ctx.db.create_book(b).await.unwrap();
    }
    // then by querying inserted n of books we get them all back correctly sorted by id
    let queried = ctx
        .db
        .get_books(0, tested_books().len() as i32)
        .await
        .unwrap();
    for (returned, expected) in queried.iter().zip(tested_books().iter()) {
        assert_eq!(returned, expected);
    }
}

fn new_book() -> NewBook {
    NewBook {
        title: "title".to_string(),
        author: "author".to_string(),
        year: "1234".to_string(),
        description: Some("description".to_string()),
        image: Some(NewImage {
            type_: ImageType::Base64,
            data: "==somebase64data".to_string(),
        }),
    }
}

fn update_book_all_fields() -> UpdateBook {
    UpdateBook {
        title: Some("updatedtitle".to_string()),
        author: Some("updatedauthor".to_string()),
        year: Some("333".to_string()),
        description: Some("description".to_string()),
        image: Some(UpdateImage {
            type_: ImageType::Url,
            data: "img1.jpg".to_string(),
        }),
    }
}

fn update_book_title_only() -> UpdateBook {
    UpdateBook {
        title: Some("updatedtitle".to_string()),
        author: None,
        year: None,
        description: None,
        image: None,
    }
}

fn update_book_image_base64_only() -> UpdateBook {
    UpdateBook {
        title: None,
        author: None,
        year: None,
        description: None,
        image: Some(UpdateImage {
            type_: ImageType::Base64,
            data: "==test".to_string(),
        }),
    }
}

fn new_books() -> Vec<NewBook> {
    vec![
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: Some("description".to_string()),
            image: Some(NewImage {
                type_: ImageType::Url,
                data: "img1.jpg".to_string(),
            }),
        },
        NewBook {
            title: "title2".to_string(),
            author: "author2".to_string(),
            year: "1235".to_string(),
            description: None,
            image: None,
        },
        NewBook {
            title: "title3".to_string(),
            author: "author3".to_string(),
            year: "1236".to_string(),
            description: None,
            image: None,
        },
        NewBook {
            title: "title4".to_string(),
            author: "author4".to_string(),
            year: "1237".to_string(),
            description: Some("desc".to_string()),
            image: None,
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: Some(NewImage {
                type_: ImageType::Url,
                data: "img1.jpg".to_string(),
            }),
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: Some(NewImage {
                type_: ImageType::Url,
                data: "=asdfaimgpg".to_string(),
            }),
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: None,
        },
    ]
}

struct TestContext {
    db: Db,
    db_name: String,
}

impl TestContext {
    async fn new() -> Self {
        let db_name = create_random_database(ADMIN_PG_POOL.get().unwrap(), BOOK_DB_TEMPLATE).await;
        let pool = get_temporary_db_pool(&db_name).await;
        let cache = Cache::new(
            Cache::connect_redis(REDIS_CONN_STR).await.unwrap(),
            Duration::from_secs(REDIS_EXPIRY_SEC),
        )
        .await;
        let event_bus = Arc::new(EventBus::new());
        Self {
            db: Db::new(pool, cache, event_bus).await.unwrap(),
            db_name,
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        let db_name = self.db_name.clone();
        std::thread::scope(|s| {
            s.spawn(|| {
                let rt = tokio::runtime::Runtime::new().unwrap();
                rt.block_on(self.db.close());
                rt.block_on(drop_database(&db_name, ADMIN_CONN_STR));
            });
        });
    }
}
