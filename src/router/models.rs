use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::db::ImageType;

/// Template which is converted into json and sent as a response to every
/// request
#[derive(Serialize)]
pub struct ApiResponse {
    pub status: u16,
    pub message: String,
    pub content: Option<Value>,
}

/// Inserted into [`ApiResponse::content`] as a response to requests that require
/// paginated content
#[derive(Serialize)]
pub struct PaginatedContent {
    pub count: i64,
    pub skip: i32,
    pub limit: i32,
    pub results: Value,
}

/// Query template for handlers that support paginated content like
/// [`super::handlers::get_books`]
#[derive(Deserialize)]
pub struct Pagination {
    pub skip: Option<i32>,
    pub limit: Option<i32>,
}

/// Query template for handlers that create or update a book and need
/// to have it passed as a parameter like [`super::handlers::create_book`]
#[cfg_attr(test, derive(Clone))]
#[derive(Deserialize, Debug)]
pub struct NewBook {
    pub title: String,
    pub author: String,
    pub year: String,
    pub description: Option<String>,
    pub image: Option<NewImage>,
}

/// Image model embedded inside [`NewBook`]
#[cfg_attr(test, derive(Clone))]
#[derive(Deserialize, Debug)]
pub struct NewImage {
    #[serde(rename = "type")]
    pub type_: ImageType,
    pub data: String,
}

#[cfg_attr(test, derive(Clone))]
#[derive(Serialize, Deserialize, Debug)]
pub struct UpdateBook {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub year: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<UpdateImage>,
}

#[cfg_attr(test, derive(Clone))]
#[derive(Serialize, Deserialize, Debug)]
pub struct UpdateImage {
    #[serde(rename = "type")]
    pub type_: ImageType,
    pub data: String,
}
