use async_graphql::{Context, Object, Result};
use std::marker::PhantomData;

use super::models::{Book, Health};
use crate::{state::AppState, traits::DatabaseHandler};

#[derive(Clone)]
pub struct QueryRoot<T>(pub PhantomData<T>);

#[Object]
impl<T> QueryRoot<T>
where
    T: DatabaseHandler + Clone + Send + Sync + 'static,
{
    async fn health(&self, ctx: &Context<'_>) -> Health {
        let services = ctx.data::<AppState<T>>().unwrap();
        Health {
            alive: true,
            db: services.db.ping().await,
        }
    }

    async fn books(
        &self,
        ctx: &Context<'_>,
        #[graphql(default = 0)] skip: i32,
        #[graphql(default = 10)] limit: i32,
    ) -> Result<Vec<Book>> {
        let services = ctx.data::<AppState<T>>()?;
        let books = services.db.get_books(skip, limit).await?;
        Ok(books.into_iter().map(std::convert::Into::into).collect())
    }

    async fn book(&self, ctx: &Context<'_>, id: i32) -> Result<Book> {
        let services = ctx.data::<AppState<T>>()?;
        let book = services.db.get_book(id).await?;
        Ok(book.into())
    }
}
