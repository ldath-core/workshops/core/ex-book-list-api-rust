use async_graphql::{Context, Object, Result};
use std::marker::PhantomData;

use super::models::{Book, NewBook, UpdateBook};
use crate::{state::AppState, traits::DatabaseHandler};

pub struct MutationRoot<T>(pub PhantomData<T>);

#[Object]
impl<T> MutationRoot<T>
where
    T: DatabaseHandler + Clone + Send + Sync + 'static,
{
    async fn create_book(&self, ctx: &Context<'_>, book: NewBook) -> Result<Book> {
        let services = ctx.data::<AppState<T>>()?;
        let res = services.db.create_book(book.into()).await?;
        Ok(res.into())
    }

    async fn update_book(&self, ctx: &Context<'_>, id: i32, book: UpdateBook) -> Result<Book> {
        let services = ctx.data::<AppState<T>>()?;
        let res = services.db.update_book(id, book.into()).await?;
        Ok(res.into())
    }

    async fn delete_book(&self, ctx: &Context<'_>, id: i32) -> Result<u64> {
        let services = ctx.data::<AppState<T>>()?;
        let res = services.db.delete_book(id).await?;
        Ok(res)
    }
}
