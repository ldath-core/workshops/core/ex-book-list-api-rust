use anyhow::Result;
use async_graphql::{futures_util::Stream, Context, Subscription};
use std::marker::PhantomData;
use tokio_stream::{wrappers::BroadcastStream, StreamExt};

use super::models::BookEvent;
use crate::{state::AppState, traits::DatabaseHandler};

pub struct SubscriptionRoot<T>(pub PhantomData<T>);

#[Subscription]
impl<T> SubscriptionRoot<T>
where
    T: DatabaseHandler + Clone + Send + Sync + 'static,
{
    /// event stream sending status of updates/creates/deletes of books in real time
    async fn book_events(&self, ctx: &Context<'_>) -> Result<impl Stream<Item = BookEvent>>
    where
        T: DatabaseHandler + Clone + Send + Sync + 'static,
    {
        let services = ctx.data::<AppState<T>>().unwrap();
        let receiver = services.event_bus.subscribe()?;
        let stream = BroadcastStream::new(receiver).map(|b| b.unwrap().into());
        Ok(stream)
    }
}
