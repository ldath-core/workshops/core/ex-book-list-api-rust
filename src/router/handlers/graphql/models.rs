use async_graphql::{Enum, InputObject, SimpleObject, ID};

use crate::{db, event_bus, router};

#[derive(Clone, Debug, InputObject)]
pub struct NewBook {
    pub title: String,
    pub author: String,
    pub year: String,
    pub description: Option<String>,
    pub image: Option<NewImage>,
}

impl From<NewBook> for router::models::NewBook {
    fn from(value: NewBook) -> Self {
        Self {
            title: value.title,
            author: value.author,
            year: value.year,
            description: value.description,
            image: value.image.map(std::convert::Into::into),
        }
    }
}

#[derive(Clone, Debug, InputObject)]
pub struct NewImage {
    pub type_: InputImageType,
    pub data: String,
}

impl From<NewImage> for router::models::NewImage {
    fn from(value: NewImage) -> Self {
        Self {
            type_: match value.type_ {
                InputImageType::Base64 => db::ImageType::Base64,
                InputImageType::Url => db::ImageType::Url,
            },
            data: value.data,
        }
    }
}

#[derive(Clone, Debug, InputObject)]
pub struct UpdateBook {
    pub title: Option<String>,
    pub author: Option<String>,
    pub year: Option<String>,
    pub description: Option<String>,
    pub image: Option<UpdateImage>,
}

impl From<UpdateBook> for router::models::UpdateBook {
    fn from(value: UpdateBook) -> Self {
        Self {
            title: value.title,
            author: value.author,
            year: value.year,
            description: value.description,
            image: value.image.map(std::convert::Into::into),
        }
    }
}

#[derive(Clone, Debug, InputObject)]
pub struct UpdateImage {
    pub type_: InputImageType,
    pub data: String,
}

impl From<UpdateImage> for router::models::UpdateImage {
    fn from(value: UpdateImage) -> Self {
        Self {
            type_: match value.type_ {
                InputImageType::Base64 => db::ImageType::Base64,
                InputImageType::Url => db::ImageType::Url,
            },
            data: value.data,
        }
    }
}

#[derive(Enum, Copy, Clone, Debug, Eq, PartialEq)]
pub enum InputImageType {
    Base64,
    Url,
}

#[derive(SimpleObject)]
pub struct Book {
    pub id: ID,
    pub title: String,
    pub author: String,
    pub year: String,
    pub description: Option<String>,
    pub image: Option<Image>,
}

impl From<db::Book> for Book {
    fn from(value: db::Book) -> Self {
        Self {
            id: ID::from(value.id),
            title: value.title,
            author: value.author,
            year: value.year,
            description: value.description,
            image: value.image.map(Image::from),
        }
    }
}

#[derive(SimpleObject)]
pub struct Image {
    pub type_: ImageType,
    pub data: String,
}

impl From<db::Image> for Image {
    fn from(value: db::Image) -> Self {
        Self {
            type_: match value.type_ {
                db::ImageType::Base64 => ImageType::Base64,
                db::ImageType::Url => ImageType::Url,
            },
            data: value.data,
        }
    }
}

#[derive(Enum, Copy, Clone, Eq, PartialEq)]
pub enum ImageType {
    Base64,
    Url,
}

#[derive(SimpleObject)]
pub struct Health {
    pub alive: bool,
    pub db: bool,
}

#[derive(Enum, Copy, Clone, Eq, PartialEq)]
pub enum BookEventType {
    Test,
    Created,
    Updated,
    Deleted,
}

#[derive(SimpleObject)]
pub struct BookEvent {
    event_type: BookEventType,
    book: Option<Book>,
    id: Option<i32>,
}

impl From<event_bus::BookEvent> for BookEvent {
    fn from(value: event_bus::BookEvent) -> Self {
        match value {
            event_bus::BookEvent::Created(v) => {
                let id = v.id;
                Self {
                    event_type: BookEventType::Created,
                    book: Some(Book::from(v)),
                    id: Some(id),
                }
            },
            event_bus::BookEvent::Deleted(v) => Self {
                event_type: BookEventType::Deleted,
                book: None,
                id: Some(v),
            },
            event_bus::BookEvent::Updated(v) => {
                let id = v.id;
                Self {
                    event_type: BookEventType::Updated,
                    book: Some(Book::from(v)),
                    id: Some(id),
                }
            },
        }
    }
}
