//! Contains handlers used by the book server
use axum::{
    extract::{Query, State},
    http::StatusCode,
    response::IntoResponse,
};
use serde_json::json;
use tracing::{debug, error, info};

use super::super::{
    macros::{response, unwrap_or_return},
    models::{ApiResponse, NewBook, PaginatedContent, Pagination, UpdateBook},
};
use super::extractors::{Json, Path};
use crate::{state::AppState, traits::DatabaseHandler};

pub async fn get_health<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
) -> impl IntoResponse {
    response!(
        StatusCode::OK,
        "book-list api health",
        Some(json!({"alive": true, "postgres": services.db.ping().await }))
    )
}

pub async fn get_books<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
    pagination: Query<Pagination>,
) -> impl IntoResponse {
    let skip = pagination.skip.unwrap_or(0);
    let limit = pagination.limit.unwrap_or(10);
    if skip < 0 || limit < 1 {
        return response!(
            StatusCode::BAD_REQUEST,
            "skip must be >= 0, limit >= 1",
            None
        );
    }
    // potential race condition, get & count is not wrapped in a transaction
    let b = unwrap_or_return!(
        services.db.get_books(skip, limit).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting books",
            None
        )
    );
    let count = unwrap_or_return!(
        services.db.count_books().await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with counting books",
            None
        )
    );
    response!(
        StatusCode::OK,
        format!("books - skip: {skip}; limit: {limit}"),
        Some(
            serde_json::to_value(PaginatedContent {
                count,
                skip,
                limit,
                results: serde_json::to_value(b).unwrap(),
            })
            .unwrap()
        )
    )
}

pub async fn create_book<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
    Json(b): Json<NewBook>,
) -> impl IntoResponse {
    info!("create_book request for {:?}", b);
    if b.title.is_empty() || b.author.is_empty() || b.year.is_empty() {
        return response!(
            StatusCode::NOT_ACCEPTABLE,
            "title, author and year cannot be empty",
            None
        );
    }
    let b = unwrap_or_return!(
        services.db.create_book(b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with creating book",
            None
        )
    );
    let j = serde_json::to_value(b).expect("serde_json couldn't hande Book struct");
    response!(StatusCode::CREATED, "ok", Some(j))
}

pub async fn get_book<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    debug!("get_book request for id: {}", id);
    let b = unwrap_or_return!(
        services.db.get_book(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting book",
            None
        )
    );
    let j = serde_json::to_value(b).expect("serde_json couldn't hande Book struct");
    response!(StatusCode::OK, "book", Some(j))
}

pub async fn update_book<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
    Path(id): Path<i32>,
    Json(b): Json<UpdateBook>,
) -> impl IntoResponse {
    debug!("update_book request for id: {} with new data {:?}", id, b);
    let r = unwrap_or_return!(
        services.db.update_book(id, b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with updating book",
            None
        )
    );
    response!(
        StatusCode::ACCEPTED,
        format!("book: {id} updated"),
        Some(serde_json::to_value(r).unwrap())
    )
}

pub async fn delete_book<D: DatabaseHandler>(
    State(services): State<AppState<D>>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    debug!("delete_book request for id: {}", id);
    let b = unwrap_or_return!(
        services.db.delete_book(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with deleting book",
            None
        )
    );
    match b {
        0 => response!(StatusCode::NOT_FOUND, "book not found", None),
        _ => response!(StatusCode::ACCEPTED, format!("book: {b} deleted"), None),
    }
}
