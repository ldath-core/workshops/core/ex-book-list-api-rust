use async_graphql::{extensions::Tracing, http::ALL_WEBSOCKET_PROTOCOLS, Schema};
use async_graphql_axum::{GraphQLProtocol, GraphQLRequest, GraphQLResponse, GraphQLWebSocket};
use axum::{
    extract::{Extension, WebSocketUpgrade},
    response::Response,
};
use std::marker::PhantomData;
use tracing::{debug, info};

use crate::{state::AppState, traits::DatabaseHandler};
use mutation::MutationRoot;
use query::QueryRoot;
use subscription::SubscriptionRoot;

pub mod models;
mod mutation;
mod query;
mod subscription;

pub type ServiceSchema<T> = Schema<QueryRoot<T>, MutationRoot<T>, SubscriptionRoot<T>>;

pub fn build_schema<D>(app_services: AppState<D>) -> ServiceSchema<D>
where
    D: DatabaseHandler + Clone + Send + Sync + 'static,
{
    let schema = Schema::build(
        QueryRoot::<D>(PhantomData),
        MutationRoot::<D>(PhantomData),
        SubscriptionRoot::<D>(PhantomData),
    )
    .data(app_services)
    .extension(Tracing)
    .finish();
    info!("built schema: {}", &schema.sdl());
    schema
}

pub async fn graphql_handler<D: DatabaseHandler + Clone + Send + Sync + 'static>(
    schema: Extension<ServiceSchema<D>>,
    req: GraphQLRequest,
) -> GraphQLResponse {
    schema.execute(req.into_inner()).await.into()
}

pub async fn graphql_ws_handler<D: DatabaseHandler + Clone + Send + Sync + 'static>(
    Extension(schema): Extension<ServiceSchema<D>>,
    protocol: GraphQLProtocol,
    websocket: WebSocketUpgrade,
) -> Response {
    debug!("starting graphql ws handler");
    websocket
        .protocols(ALL_WEBSOCKET_PROTOCOLS)
        .on_upgrade(move |stream| {
            GraphQLWebSocket::new(stream, schema.clone(), protocol)
                // .on_connection_init(on_connection_init)
                .serve()
        })
}
