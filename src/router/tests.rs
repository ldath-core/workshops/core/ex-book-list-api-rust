use std::sync::Arc;

use super::*;
use crate::{
    db::{Book, Image, ImageType},
    event_bus::EventBus,
    tests::utils::{body_as_json, body_as_string},
    traits::{MockDatabaseHandler, MockDatabaseHandlerWrapper},
};

use anyhow::Error;
use axum::{body::Body, http::Request};
use http::StatusCode;
use serde_json::{json, Value};
use tower::ServiceExt; // for `oneshot`

#[tokio::test]
async fn test_get_health() {
    let expected_status = StatusCode::OK;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_ping().returning(|| true);

    let req = Request::get("/v1/health").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);
}

#[tokio::test]
async fn test_wrong_uri() {
    let expected_status = StatusCode::NOT_FOUND;

    let req = Request::get("/nonexistent").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);
}

#[tokio::test]
async fn test_get_books_no_pagination() {
    let expected_status = StatusCode::OK;
    let tested_books = get_vec_books;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_count_books()
        .returning(move || Ok(tested_books().len() as i64));
    mock_db
        .expect_get_books()
        .returning(move |_, _| Ok(tested_books()));

    let req = Request::get("/v1/books").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    for (returned, expected) in body["content"]["results"]
        .as_array()
        .unwrap()
        .iter()
        .zip(tested_books())
    {
        let expect = serde_json::to_value(expected).unwrap();
        for (k, v) in expect.as_object().unwrap().iter() {
            assert_eq!(&returned[k], v);
        }
    }
}

#[tokio::test]
async fn test_pagination_only_skip() {
    let expected_status = StatusCode::OK;
    let skip = 1337;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_books().returning(move || Ok(0));
    mock_db
        .expect_get_books()
        .returning(move |_, _| Ok(Vec::<Book>::new()));

    let req = Request::get(format!("/v1/books?skip={skip}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"]["skip"], skip);
}

#[tokio::test]
async fn test_pagination_only_limit() {
    let expected_status = StatusCode::OK;
    let limit = 1337;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_books().returning(move || Ok(0));
    mock_db
        .expect_get_books()
        .returning(move |_, _| Ok(Vec::<Book>::new()));

    let req = Request::get(format!("/v1/books?limit={limit}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"]["limit"], limit);
}

#[tokio::test]
async fn test_pagination_limit_below_one() {
    let expected_status = StatusCode::BAD_REQUEST;
    let limit = 0;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_count_books().returning(move || Ok(0));
    mock_db
        .expect_get_books()
        .returning(move |_, _| Ok(Vec::<Book>::new()));

    let req = Request::get(format!("/v1/books?limit={limit}"))
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_get_existing_book() {
    let expected_status = StatusCode::OK;
    let tested_book = get_book;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_book()
        .returning(move |_| Ok(tested_book()));

    let req = Request::get("/v1/books/1").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());

    let original = serde_json::to_value(tested_book()).unwrap();
    for (k, v) in original.as_object().unwrap().iter() {
        assert_eq!(&body["content"][k], v);
    }
}

#[tokio::test]
async fn test_get_book_with_null_values() {
    let expected_status = StatusCode::OK;
    let tested_book = get_book_with_nones;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_book()
        .returning(move |_| Ok(tested_book()));

    let req = Request::get("/v1/books/1").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_string(resp).await;
    assert!(!body.contains(":null,"));
}

#[tokio::test]
async fn test_create_book() {
    let expected_status = StatusCode::CREATED;
    let tested_book = get_book;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_create_book()
        .returning(move |_| Ok(tested_book()));

    let req = Request::post("/v1/books")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(&book_as_json(tested_book())).unwrap(),
        ))
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());

    let original = serde_json::to_value(tested_book()).unwrap();
    for (k, v) in original.as_object().unwrap().iter() {
        assert_eq!(&body["content"][k], v);
    }
}

#[tokio::test]
async fn test_create_book_empty_fields() {
    let expected_status = StatusCode::NOT_ACCEPTABLE;
    let tested_book_json = get_newbook_empty_json;

    let router = get_router(wrap_mock(MockDatabaseHandler::new()));

    let req = Request::post("/v1/books")
        .header("content-type", "application/json")
        .body(Body::from(serde_json::to_vec(&tested_book_json()).unwrap()))
        .unwrap();
    let resp = router.oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
}

#[tokio::test]
async fn test_create_book_wrong_json() {
    let expected_status = StatusCode::BAD_REQUEST;

    let router = get_router(wrap_mock(MockDatabaseHandler::new()));

    let req = Request::post("/v1/books")
        .header("content-type", "application/json")
        .body(Body::from("{{}{ Not a correct json"))
        .unwrap();
    let resp = router.oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_get_non_existent_book() {
    let expected_status = StatusCode::INTERNAL_SERVER_ERROR;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_get_book()
        .returning(|_| Err(Error::new(sqlx::Error::RowNotFound)));

    let req = Request::get("/v1/books/1").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_update_book() {
    let expected_status = StatusCode::ACCEPTED;
    let tested_book = get_book;

    let mut mock_db = MockDatabaseHandler::new();
    mock_db
        .expect_update_book()
        .returning(move |_, _| Ok(tested_book()));

    let req = Request::put("/v1/books/1")
        .header("content-type", "application/json")
        .body(Body::from(
            serde_json::to_vec(&book_as_json(tested_book())).unwrap(),
        ))
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    for (k, v) in book_as_json(tested_book()).as_object().unwrap().iter() {
        assert_eq!(&body["content"][k], v);
    }
}

#[tokio::test]
async fn test_update_book_wrong_json() {
    let expected_status = StatusCode::BAD_REQUEST;

    let req = Request::put("/v1/books/1")
        .header("content-type", "application/json")
        .body(Body::from("{{}{ Not a correct json"))
        .unwrap();
    let resp = get_router(wrap_mock(MockDatabaseHandler::new()))
        .oneshot(req)
        .await
        .unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["content"], Value::Null);
}

#[tokio::test]
async fn test_delete_book() {
    let expected_status = StatusCode::ACCEPTED;
    let expected_message = "book: 1 deleted";

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_delete_book().returning(|_| Ok(1));

    let req = Request::delete("/v1/books/1")
        .header("content-type", "application/json")
        .body(Body::empty())
        .unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["message"], expected_message);
}

#[tokio::test]
async fn test_delete_non_existent_book() {
    let expected_status = StatusCode::NOT_FOUND;
    let expected_message = "book not found";

    let mut mock_db = MockDatabaseHandler::new();
    mock_db.expect_delete_book().returning(|_| Ok(0));

    let req = Request::delete("/v1/books/1").body(Body::empty()).unwrap();
    let resp = get_router(wrap_mock(mock_db)).oneshot(req).await.unwrap();

    assert_eq!(resp.status(), expected_status);

    let body = body_as_json(resp).await;

    assert_eq!(body["status"], expected_status.as_u16());
    assert_eq!(body["message"], expected_message);
}

fn get_vec_books() -> Vec<Book> {
    vec![
        Book {
            id: 1,
            title: "book1".to_string(),
            author: "author1".to_string(),
            year: "2137".to_string(),
            description: None,
            image: None,
        },
        Book {
            id: 2,
            title: "book2".to_string(),
            author: "author2".to_string(),
            year: "2138".to_string(),
            description: None,
            image: Some(Image {
                type_: ImageType::Url,
                data: "img1.jpg".to_string(),
            }),
        },
        Book {
            id: 4,
            title: "book3".to_string(),
            author: "author3".to_string(),
            year: "2139".to_string(),
            description: Some("desc".to_string()),
            image: Some(Image {
                type_: ImageType::Url,
                data: "img2.jpg".to_string(),
            }),
        },
        Book {
            id: 1_000_000,
            title: "book4".to_string(),
            author: "author4".to_string(),
            year: "2140".to_string(),
            description: None,
            image: Some(Image {
                type_: ImageType::Base64,
                data: "==somebase64data".to_string(),
            }),
        },
    ]
}

fn get_book() -> Book {
    Book {
        id: 1,
        title: "book1".to_string(),
        author: "author1".to_string(),
        year: "2137".to_string(),
        description: Some("desc1".to_string()),
        image: Some(Image {
            type_: ImageType::Url,
            data: "img2.jpg".to_string(),
        }),
    }
}

fn get_book_with_nones() -> Book {
    Book {
        id: 1,
        title: "book1".to_string(),
        author: "author1".to_string(),
        year: "2137".to_string(),
        description: None,
        image: None,
    }
}

fn book_as_json(b: Book) -> Value {
    serde_json::to_value(b).unwrap()
}

fn get_newbook_empty_json() -> Value {
    json!({ "title": "", "author": "", "year": "" })
}

fn wrap_mock(m: MockDatabaseHandler) -> AppState<MockDatabaseHandlerWrapper> {
    let event_bus = Arc::new(EventBus::new());
    let wrapped_mock = MockDatabaseHandlerWrapper::new(m);

    AppState::new(event_bus, wrapped_mock)
}
