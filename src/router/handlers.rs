//! Contains handlers used by the book server
use axum::{
    http::{Request, StatusCode},
    middleware::Next,
    response::IntoResponse,
};

use super::models::ApiResponse;

mod extractors;
pub mod graphql;
pub mod rest;

pub(super) async fn not_found() -> impl IntoResponse {
    (
        StatusCode::NOT_FOUND,
        axum::Json(ApiResponse {
            status: StatusCode::NOT_FOUND.as_u16(),
            message: "Page Not Found".to_owned(),
            content: None,
        }),
    )
}

pub async fn method_not_allowed<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let resp = next.run(req).await;
    let status = resp.status();
    match status {
        StatusCode::METHOD_NOT_ALLOWED => (
            StatusCode::METHOD_NOT_ALLOWED,
            axum::Json(ApiResponse {
                status: StatusCode::METHOD_NOT_ALLOWED.as_u16(),
                message: "Method not allowed".to_owned(),
                content: None,
            }),
        )
            .into_response(),
        _ => resp,
    }
}
