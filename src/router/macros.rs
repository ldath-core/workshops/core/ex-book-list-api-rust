/// Creates a [response](axum::response::IntoResponse) containing
/// [`ApiResponse`](super::models::ApiResponse)
/// from its parts
///
/// # Arguments
///
/// * `status` - [`http::StatusCode`]
/// * `message` - message as an &str to be included in the response
/// * `content` - object serializable by `serde_json`
///
/// # Panics
///
/// Types not matching `ApiResponse`?
///
/// # Examples
///
/// ```ignore
/// response!(
///     StatusCode::OK,
///     "Message",
///     Some(json!({"test": 1}))
/// );
/// async fn axum_handler() -> impl axum::response::IntoResponse {
///     response!(StatusCode::OK, "Message", None)
/// }
/// ```
macro_rules! response {
    ($status:expr, $message:expr, $content:expr) => {
        (
            $status,
            axum::Json(ApiResponse {
                status: $status.as_u16(),
                message: $message.to_owned(),
                content: $content,
            }),
        )
    };
}

/// Unwraps the input object if it's an Ok or logs the error to [`tracing::error`]
/// and return from overlaying function with $ret
///
/// # Arguments
///
/// * `in` - [Result] to be unwrapped
/// * `ret` - value to be returned
///
/// # Panics
///
/// Prob when is not a Result or ret is uncompatible with func but would have to verify
///
/// # Examples
///
/// ```ignore
/// async fn example() -> impl axum::response::IntoResponse {
///     let b = unwrap_or_return!(
///         Ok("str"),
///         response!(StatusCode::INTERNAL_SERVER_ERROR, "Problem with getting book", None)
///     );
/// };
/// ```
macro_rules! unwrap_or_return {
    ($in: expr, $ret: expr) => {
        match $in {
            Ok(val) => val,
            Err(err) => {
                error!("Error: {}", err);
                return $ret;
            },
        }
    };
}

pub(super) use response;
pub(super) use unwrap_or_return;
