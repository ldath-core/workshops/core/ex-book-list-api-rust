//! TODO custom error types instead of anyhow
//!
use anyhow::Result;
use async_trait::async_trait;
#[cfg(test)]
use mockall::predicate::str;
use sqlx::{
    migrate::Migrator,
    prelude::*,
    {query, query_as, PgPool},
};
use std::{path::Path, sync::Arc};
use tracing::{debug, error, info, instrument};

use crate::{
    event_bus::{BookEvent, EventBus},
    router::{NewBook, UpdateBook},
    traits::{BooksRepo, DatabaseConnector, DatabaseHandler},
};
use cache::Cache;
pub use models::{Book, Image, ImageType};

pub mod cache;
mod models;
#[cfg(test)]
mod tests;

const MIGRATIONS_PATH: &str = "./migrations";

/// Contains DB connection and implements layer for interacting with books database
#[derive(Debug, Clone)]
pub struct Db {
    pool: PgPool,
    cache: Cache,
    event_bus: Arc<EventBus>,
}

impl Db {
    #[instrument(skip(cache, event_bus))]
    pub async fn new(pool: PgPool, cache: Cache, event_bus: Arc<EventBus>) -> Result<Self> {
        Ok(Self {
            pool,
            cache,
            event_bus,
        })
    }

    #[instrument(skip(conn_str))]
    pub async fn connect_pool(conn_str: &str) -> Result<PgPool> {
        debug!("postgres connection string: {}", conn_str);
        let pool = sqlx::PgPool::connect(conn_str).await?;
        info!("Connected to postgres");
        Ok(pool)
    }

    /// Drops table books and related
    #[instrument]
    async fn drop_schema(&self) -> Result<()> {
        // Ignore errors occuring in case of missing migration
        let _ = query("DROP TYPE image_type CASCADE")
            .execute(&self.pool)
            .await;
        let _ = query("DROP TYPE image CASCADE").execute(&self.pool).await;
        let _ = query("DROP TABLE books_id_seq").execute(&self.pool).await;
        let _ = query("DROP TABLE _sqlx_migrations")
            .execute(&self.pool)
            .await;
        let q = query("DROP TABLE books").execute(&self.pool).await;
        if let Err(e) = q {
            error!("couldn't drop table err: {}", e);
        }
        debug!("Dropped books tables");
        let _ = self.cache.flush().await;
        Ok(())
    }
}

/// Abstract away DB internals exposing interface for interacting with books data
#[async_trait]
impl BooksRepo for Db {
    #[instrument]
    async fn count_books(&self) -> Result<i64> {
        let q = query("SELECT COUNT(*) FROM books")
            .fetch_one(&self.pool)
            .await?;
        let count: i64 = q.get("count");
        debug!("count of books: {:?}", count);
        Ok(count)
    }

    #[instrument]
    async fn get_books(&self, skip: i32, limit: i32) -> Result<Vec<Book>> {
        let cache_key = format!("books:{skip}:{limit}");
        if let Ok(books) = self.cache.get(&cache_key).await {
            return Ok(books);
        }

        let q = query_as::<_, Book>(
            r#"SELECT id, title, author, year, description, image
            FROM books
            LIMIT $1
            OFFSET $2"#,
        )
        .bind(limit)
        .bind(skip)
        .fetch_all(&self.pool)
        .await?;
        debug!("all books: {:?}", q);

        let _ = self.cache.set(&cache_key, &q).await;

        Ok(q)
    }

    #[instrument]
    async fn get_book(&self, id: i32) -> Result<Book> {
        let cache_key = format!("book:{id}");
        if let Ok(book) = self.cache.get(&cache_key).await {
            return Ok(book);
        }

        let q = query_as::<_, Book>(
            r#"SELECT id, title, author, year, description, image
            FROM books
            WHERE id = $1"#,
        )
        .bind(id)
        .fetch_one(&self.pool)
        .await?;
        debug!("book with id {}: {:?}", id, q);

        let _ = self.cache.set(&cache_key, &q).await;

        Ok(q)
    }

    #[instrument]
    async fn create_book(&self, b: NewBook) -> Result<Book> {
        debug!("creating book {:?}", b);
        let q = query_as::<_, Book>(
            r#"INSERT INTO books(title, author, year, description, image)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING *"#,
        )
        .bind(b.title)
        .bind(b.author)
        .bind(b.year)
        .bind(b.description)
        .bind(b.image.map(Image::from))
        .fetch_one(&self.pool)
        .await?;

        debug!("book returned from query: {q:?}");
        let _ = self
            .event_bus
            .publish(BookEvent::Created(q.clone()))
            .map_err(|e| error!("couldn't publish created event to the bus, {e}"));

        Ok(q)
    }

    #[instrument]
    async fn delete_book(&self, id: i32) -> Result<u64> {
        let cache_key = format!("book:{id}");

        debug!("deleting book {} from DB", id);
        let q = query(
            r#"DELETE FROM books
            WHERE id = $1"#,
        )
        .bind(id)
        .execute(&self.pool)
        .await?;
        let rows = q.rows_affected();
        debug!("Deleted {} rows from the DB", rows);
        if rows > 0 {
            let _ = self
                .event_bus
                .publish(BookEvent::Deleted(id))
                .map_err(|e| error!("couldn't publish deleted event to the bus, {e}"));
            let _ = self.cache.delete(&cache_key).await;
        }

        Ok(rows)
    }

    #[instrument]
    async fn update_book(&self, id: i32, b: UpdateBook) -> Result<Book> {
        let cache_key = format!("book:{id}");

        debug!("updating book {:?} into db, new ID: {}", b, id);
        let q = query_as::<_, Book>(
            r#"UPDATE books SET
                title = COALESCE($1, title),
                author = COALESCE($2, author),
                year = COALESCE($3, year),
                description = COALESCE($4, description),
                image = COALESCE($5, image)
            WHERE id = $6
            RETURNING *"#,
        )
        .bind(b.title)
        .bind(b.author)
        .bind(b.year)
        .bind(b.description)
        .bind(b.image.map(Image::from))
        .bind(id)
        .fetch_one(&self.pool)
        .await?;

        let _ = self
            .event_bus
            .publish(BookEvent::Updated(q.clone()))
            .map_err(|e| error!("couldn't publish updated event to the bus, {e}"));
        let _ = self.cache.delete(&cache_key).await;

        Ok(q)
    }
}

#[async_trait]
impl DatabaseConnector for Db {
    /// Deletes all currently existing data from books then inserts test data into
    /// the table
    #[instrument]
    async fn create_test_data(&self) -> Result<()> {
        let mut tx = self.pool.begin().await?;
        let q = query("DELETE FROM books;").execute(&mut tx).await?;
        debug!("Deleted {} entries from books", q.rows_affected());

        let rows = vec![
            ("Golang is great", "Mr. Great", "2012", None, None),
            ("C++ is greatest", "Mr. C++", "2015", Some("desc"), None),
            (
                "C++ is very old",
                "Mr. Old",
                "2014",
                Some("true"),
                Some(Image {
                    type_: ImageType::Base64,
                    data: "==test".to_string(),
                }),
            ),
        ];
        for row in rows {
            query(
                r#"
                INSERT INTO books(title, author, year, description, image)
                VALUES ($1, $2, $3, $4, $5)
                RETURNING *;
                "#,
            )
            .bind(row.0)
            .bind(row.1)
            .bind(row.2)
            .bind(row.3)
            .bind(row.4)
            .execute(&mut tx)
            .await?;
        }
        tx.commit().await?;
        info!("Cleared books then insterted sample data");
        Ok(())
    }

    /// Sets up necessary underlaying database schema and tables
    #[instrument]
    async fn migrate_db(&self) -> Result<()> {
        self.drop_schema().await?;
        let mut m = Migrator::new(Path::new(MIGRATIONS_PATH))
            .await
            .expect("Couldn't initialize the migrator");
        m.set_ignore_missing(true);
        m.run(&self.pool).await?;
        debug!("Database migrated");
        Ok(())
    }

    /// Closes DB connection, makes self unusable
    #[instrument]
    async fn close(&self) {
        self.pool.close().await;
    }

    /// Checks if DB is alive
    #[instrument]
    async fn ping(&self) -> bool {
        let check = self.pool.execute("SELECT 1").await;
        if let Err(e) = check {
            error!("couldn't ping the database, got error: {}", e);
            return false;
        }
        true
    }
}

impl DatabaseHandler for Db {}
