// TODO AppState with Db and Arc<EventBus> here
use crate::{event_bus::EventBus, traits::DatabaseHandler};
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct AppState<D>
where
    D: DatabaseHandler,
{
    pub event_bus: Arc<EventBus>,
    pub db: D,
}

impl<D> AppState<D>
where
    D: DatabaseHandler,
{
    pub fn new(event_bus: Arc<EventBus>, db: D) -> Self {
        Self { event_bus, db }
    }
}
