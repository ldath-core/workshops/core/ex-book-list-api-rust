use assert_fs::fixture::NamedTempFile;
use assert_fs::prelude::*;

use super::*;

#[test]
fn test_load_config_basic() {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(
            r#"---
env: dev
server:
    data:
postgres:
    url: postgres://book-list:password123@localhost:5432/book-list
redis:
    url: redis://:password123@localhost:5432
    key_expiry_seconds: 60
logger:
    level: debug
"#,
        )
        .unwrap();
    let c = load_config(tmp_cfg.path());
    assert!(c.env == "dev");
    assert!(c.server.data.is_empty());
    assert!(c.postgres.url == "postgres://book-list:password123@localhost:5432/book-list");
    assert_eq!(c.redis.key_expiry_seconds, 60);
    assert!(c.logger.level == "debug");
    assert!(c.sentry.is_none());
}

#[test]
fn test_load_config_with_sentry() {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(
            r#"---
env: dev
server:
    data:
postgres:
    url: postgres://book-list:password123@localhost:5432/book-list
redis:
    url: redis://:password123@localhost:5432
    key_expiry_seconds: 60
logger:
    level: debug
sentry:
    dsn: https://public@sentry.example.com/1
    environment: test
"#,
        )
        .unwrap();
    let c = load_config(tmp_cfg.path());
    assert!(c.env == "dev");
    assert!(c.server.data.is_empty());
    assert!(c.postgres.url == "postgres://book-list:password123@localhost:5432/book-list");
    assert!(c.logger.level == "debug");
    let sentry = c.sentry.unwrap();
    assert!(sentry.dsn == "https://public@sentry.example.com/1");
    assert!(sentry.environment == "test");
}
