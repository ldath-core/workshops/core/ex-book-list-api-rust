use clap::{Args, CommandFactory, Parser, Subcommand, ValueEnum};
use clap_complete::{generate, Shell};
use std::{
    net::{IpAddr, Ipv4Addr},
    path::PathBuf,
};

/// Reads args passed through the shell or prints help output
/// returns Cli struct accepted by App
#[must_use]
pub fn parse_args() -> Cli {
    Cli::parse()
}

/// Prints out completion for detected shell or bash to stdout
pub fn generate_completion() {
    let mut cmd = Cli::command();
    let cmd_name = cmd.get_name().to_string();
    let sh = Shell::from_env().unwrap_or(Shell::Bash);
    generate(sh, &mut cmd, cmd_name, &mut std::io::stdout());
}

/// Parseable struct with config parameters for App
#[derive(Parser, Debug)]
#[command(name = "book-list")]
#[command(author = "Piotr Konkol", version = "0.1.0")]
#[command(
    about = "Short description",
    long_about = "A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application."
)]
#[command(propagate_version = true)]
pub struct Cli {
    /// Path to configuration file for the app.
    #[arg(short, long, default_value = "./secrets/local.env.yaml")]
    pub config: PathBuf,

    /// Verbosity level for tracing (eg. -vv).
    /// Overwrites value present in the config file
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,

    /// Configure global logging format
    #[arg(long, value_enum, default_value = "json")]
    pub logger_format: LoggerFormat,

    /// Command deciding main action of the app
    #[command(subcommand)]
    pub command: Commands,
}

/// Command supported by the App
#[derive(Subcommand, Debug)]
pub enum Commands {
    /// A brief description of your command
    Load,
    /// A brief description of your command
    Migrate,
    /// A brief description of your command
    Completion,
    /// A brief description of your command
    Serve(Serve),
}

/// Parameters for serve command
#[derive(Args, Debug)]
pub struct Serve {
    /// This flag sets the IP to bind for our API server - overwrites config
    #[arg(short, long, default_value_t = IpAddr::V4(Ipv4Addr::new(127,0,0,1)))]
    pub bind: IpAddr,

    /// This decide if we should enable cors - in the Prod NGiNX is solving cors for us
    #[arg(short, long, default_value_t = false)]
    pub cors: bool,

    /// This decide if we should load test data or not when starting
    #[arg(short, long, default_value_t = false)]
    pub load: bool,

    /// This decide if we should migrate or not when starting
    #[arg(short, long, default_value_t = false)]
    pub migrate: bool,

    /// This flag sets the port of our API server - overwrites config
    #[arg(short, long, value_parser = clap::value_parser!(u16).range(1..), default_value_t = 8080)]
    pub port: u16,
}

/// Completion
#[derive(Args, Debug)]
pub struct Completion {
    /// This flag allows to manually specify shell for completion
    #[arg(short, long)]
    shell: Option<Shell>,
}

/// Style of the logging output
/// Default is Json
#[derive(Debug, Clone, ValueEnum)]
pub enum LoggerFormat {
    /// logs to stdout in json format
    Json,
    /// logs to stdout in pretty verbose text format
    Pretty,
    /// logs to stdout in compact text format
    Compact,
    /// logs to stdout in default tesxt format
    Full,
}
