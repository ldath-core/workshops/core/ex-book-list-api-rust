use once_cell::sync::OnceCell;
use sqlx::{query, PgPool};
use std::{sync::Arc, time::Duration};

mod graphql;
mod rest;
pub mod utils;

use crate::{
    db::{cache::Cache, Db},
    event_bus::EventBus,
    traits::DatabaseConnector,
};
use utils::{create_random_database, drop_database_forced, get_temporary_db_pool};

pub const BOOK_MAIN_TEMPLATE: &str = "books_test_template";
pub const INITIALIZE_SUBSCRIBER: bool = false;
pub const ADMIN_CONN_STR: &str = "postgres://book-list:2vQU45haQCnDS8sO@localhost:5432/book-list";
pub const REDIS_CONN_STR: &str = "redis://:devRedisPassword@127.0.0.1:6379";
pub const REDIS_EXPIRY_SEC: u64 = 0;
pub const LISTEN_IP: &str = "127.0.0.1";

#[allow(clippy::redundant_closure)]
static ADMIN_PG_POOL: OnceCell<PgPool> = OnceCell::new();

/// creates template database later used in TextContext for creating individual copies
/// for each testcase
fn suite_setup() {
    if INITIALIZE_SUBSCRIBER {
        tracing_subscriber::fmt()
            .with_max_level(tracing::Level::TRACE)
            .init();
    };
    let r = tokio::runtime::Runtime::new().unwrap();
    r.block_on(async {
        let admin_pool = sqlx::PgPool::connect(ADMIN_CONN_STR).await.unwrap();
        ADMIN_PG_POOL.get_or_init(|| admin_pool);

        query(&format!(
            " UPDATE pg_database SET datistemplate='false' WHERE datname='{BOOK_MAIN_TEMPLATE}' "
        ))
        .execute(ADMIN_PG_POOL.get().unwrap())
        .await
        .unwrap();

        query(&format!("DROP DATABASE IF EXISTS {BOOK_MAIN_TEMPLATE};"))
            .execute(ADMIN_PG_POOL.get().unwrap())
            .await
            .unwrap();

        query(&format!(
            "CREATE DATABASE {BOOK_MAIN_TEMPLATE} WITH is_template TRUE;"
        ))
        .execute(ADMIN_PG_POOL.get().unwrap())
        .await
        .unwrap();

        let template_pool = get_temporary_db_pool(BOOK_MAIN_TEMPLATE).await;
        let cache = Cache::new(
            Cache::connect_redis(REDIS_CONN_STR).await.unwrap(),
            Duration::from_secs(REDIS_EXPIRY_SEC),
        )
        .await;
        let event_bus = Arc::new(EventBus::new());
        let db = Db::new(template_pool, cache, event_bus).await.unwrap();
        db.migrate_db().await.unwrap();
        db.create_test_data().await.unwrap();
    });
}

/// creates database with a random name using the global connection pool
/// when dropped it removes the database
struct TestContext {
    db_name: String,
}

impl TestContext {
    async fn new() -> Self {
        Self {
            db_name: create_random_database(ADMIN_PG_POOL.get().unwrap(), BOOK_MAIN_TEMPLATE).await,
        }
    }
}

impl Drop for TestContext {
    fn drop(&mut self) {
        let db_name = self.db_name.clone();
        std::thread::scope(|s| {
            s.spawn(|| {
                let rt = tokio::runtime::Runtime::new().unwrap();
                rt.block_on(drop_database_forced(&db_name, ADMIN_CONN_STR));
            });
        });
    }
}
