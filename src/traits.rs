use anyhow::Result;
use async_trait::async_trait;
#[cfg(test)]
use mockall::{mock, predicate::str};

use crate::{
    db::Book,
    router::{NewBook, UpdateBook},
};

/// Represents database subpart of App
#[async_trait]
pub trait DatabaseHandler: DatabaseConnector + BooksRepo {}

/// Represents interface for interacting with books database
#[async_trait]
pub trait BooksRepo {
    async fn count_books(&self) -> Result<i64>;
    async fn get_books(&self, skip: i32, limit: i32) -> Result<Vec<Book>>;
    async fn get_book(&self, id: i32) -> Result<Book>;
    async fn create_book(&self, b: NewBook) -> Result<Book>;
    async fn delete_book(&self, id: i32) -> Result<u64>;
    async fn update_book(&self, id: i32, b: UpdateBook) -> Result<Book>;
}

/// Represents interface for initialization and managemend of the database connection
#[async_trait]
pub trait DatabaseConnector {
    async fn create_test_data(&self) -> Result<()>;
    async fn migrate_db(&self) -> Result<()>;
    async fn ping(&self) -> bool;
    async fn close(&self);
}

#[cfg(test)]
mock! {
    pub DatabaseHandler {}
    impl DatabaseHandler for DatabaseHandler {}
    #[async_trait]
    impl DatabaseConnector for DatabaseHandler {
        async fn create_test_data(&self) -> Result<()>;
        async fn migrate_db(&self) -> Result<()>;
        async fn ping(&self) -> bool;
        async fn close(&self);
    }
    #[async_trait]
    impl BooksRepo for DatabaseHandler {
        async fn count_books(&self) -> Result<i64>;
        async fn get_books(&self, skip: i32, limit: i32) -> Result<Vec<Book>>;
        async fn get_book(&self, id: i32) -> Result<Book>;
        async fn create_book(&self, b: NewBook) -> Result<Book>;
        async fn delete_book(&self, id: i32) -> Result<u64>;
        async fn update_book(&self, id: i32, b: UpdateBook) -> Result<Book>;
    }
}

#[cfg(test)]
use std::sync::Arc;
#[cfg(test)]
pub struct MockDatabaseHandlerWrapper {
    inner: Arc<MockDatabaseHandler>,
}
#[cfg(test)]
impl MockDatabaseHandlerWrapper {
    pub fn new(inner: MockDatabaseHandler) -> MockDatabaseHandlerWrapper {
        MockDatabaseHandlerWrapper {
            inner: Arc::new(inner),
        }
    }
}
#[cfg(test)]
impl Clone for MockDatabaseHandlerWrapper {
    fn clone(&self) -> Self {
        MockDatabaseHandlerWrapper {
            inner: self.inner.clone(),
        }
    }
}
#[cfg(test)]
impl DatabaseHandler for MockDatabaseHandlerWrapper {}
#[cfg(test)]
#[async_trait]
impl BooksRepo for MockDatabaseHandlerWrapper {
    async fn count_books(&self) -> Result<i64> {
        self.inner.count_books().await
    }
    async fn get_books(&self, skip: i32, limit: i32) -> Result<Vec<Book>> {
        self.inner.get_books(skip, limit).await
    }
    async fn get_book(&self, id: i32) -> Result<Book> {
        self.inner.get_book(id).await
    }
    async fn create_book(&self, b: NewBook) -> Result<Book> {
        self.inner.create_book(b).await
    }
    async fn delete_book(&self, id: i32) -> Result<u64> {
        self.inner.delete_book(id).await
    }
    async fn update_book(&self, id: i32, b: UpdateBook) -> Result<Book> {
        self.inner.update_book(id, b).await
    }
}
#[cfg(test)]
#[async_trait]
impl DatabaseConnector for MockDatabaseHandlerWrapper {
    async fn create_test_data(&self) -> Result<()> {
        Ok(())
    }
    async fn migrate_db(&self) -> Result<()> {
        Ok(())
    }
    async fn ping(&self) -> bool {
        self.inner.ping().await
    }
    async fn close(&self) {}
}
