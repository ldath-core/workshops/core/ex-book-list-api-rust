use anyhow::Result;
use std::sync::Arc;
use tokio::sync::broadcast::{self, Receiver, Sender};
use tracing::info;

use crate::db::Book;

const EVENT_BUS_CAPACITY: usize = 100000;

#[derive(Clone, Debug)]
pub enum BookEvent {
    Created(Book),
    Updated(Book),
    Deleted(i32),
}

#[derive(Debug, Clone)]
pub struct EventBus {
    tx: Sender<BookEvent>,
    _rx: Arc<Receiver<BookEvent>>,
}

impl EventBus {
    pub fn new() -> Self {
        let (tx, rx) = broadcast::channel::<BookEvent>(EVENT_BUS_CAPACITY);
        info!("created event bus with sender: {:?}", tx);
        EventBus {
            tx,
            _rx: Arc::new(rx),
        }
    }
    pub fn publish(&self, e: BookEvent) -> Result<()> {
        let _x = self.tx.send(e)?;
        Ok(())
    }

    pub fn subscribe(&self) -> Result<Receiver<BookEvent>> {
        let t = self.tx.subscribe();
        Ok(t)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn publish_created_event() {
        let tested_book = get_book;

        let event_bus = EventBus::new();

        let mut rx = event_bus.subscribe().unwrap();
        event_bus
            .publish(BookEvent::Created(tested_book()))
            .unwrap();

        let received_event = rx.recv().await.unwrap();
        let BookEvent::Created(b) = received_event else { panic!() };

        assert_eq!(tested_book(), b);
    }

    fn get_book() -> Book {
        Book {
            id: 1,
            title: "title".into(),
            author: "author".into(),
            year: "1234".into(),
            description: Some("description".into()),
            image: None,
        }
    }
}
