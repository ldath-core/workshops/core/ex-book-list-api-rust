#![forbid(unsafe_code)]
#![deny(clippy::all)]
//! book-list is an API server for performing CRUD
//! operations on a simple book data scheme.
//!
//! Implemented for the sake of [microservices-workshop-be](https://gitlab.com/mobica-workshops/documentation/microservices-backend-local-development-guide)
//!
//! It's based on [ex-book-list-api-go](https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list)
//!

use anyhow::Result;
use event_bus::EventBus;
use state::AppState;
use std::{borrow::Cow, net::SocketAddr, sync::Arc, time::Duration};
use tracing::{debug, error, info, Level};
use tracing_subscriber::{fmt::writer::MakeWriterExt, layer::SubscriberExt};

use crate::{
    cli::{generate_completion, parse_args, Commands, LoggerFormat},
    config::{load_config, SentryConfig},
    db::{cache::Cache, Db},
    router::{add_cors_layer, get_router},
    traits::DatabaseConnector,
};

mod cli;
mod config;
mod db;
mod event_bus;
mod router;
mod state;
#[cfg(test)]
mod tests;
mod traits;

#[tokio::main]
async fn main() -> Result<()> {
    let cli = parse_args();
    let config = load_config(cli.config.as_path());
    let _sentry_guard = init_logger(
        cli.verbose,
        &cli.logger_format,
        &config.logger.level,
        config.sentry,
    );

    let event_bus = Arc::new(EventBus::new());
    let db = Db::new(
        Db::connect_pool(&config.postgres.url)
            .await
            .expect("Couldn't connect to the database with url from config"),
        Cache::new(
            Cache::connect_redis(&config.redis.url)
                .await
                .expect("Couldn't connect to cache with url from config"),
            Duration::from_secs(config.redis.key_expiry_seconds),
        )
        .await,
        event_bus.clone(),
    )
    .await?;
    let app_services = AppState::new(event_bus, db.clone());
    let mut router = get_router(app_services);

    match &cli.command {
        Commands::Load => {
            db.create_test_data().await?;
            info!("Loaded test data into DB");
        },
        Commands::Migrate => {
            db.migrate_db().await?;
            info!("Migrated DB");
        },
        Commands::Serve(s) => {
            debug!("Serve params: {:?}", s);
            if s.cors {
                router = add_cors_layer(router);
            }
            if s.migrate {
                db.migrate_db().await?;
            }
            if s.load {
                db.create_test_data().await?;
            }

            let addr = SocketAddr::from((s.bind, s.port));
            let sentry_layer = tower::ServiceBuilder::new()
                .layer(sentry::integrations::tower::SentryHttpLayer::with_transaction())
                .layer(sentry::integrations::tower::NewSentryLayer::new_from_top());
            let router = router.layer(sentry_layer);

            let server = axum::Server::bind(&addr)
                .serve(router.into_make_service())
                .with_graceful_shutdown(router::shutdown_signal());
            if let Err(e) = server.await {
                error!("Server returned error {:?}", e);
            }
            db.close().await;
        },
        Commands::Completion => {
            generate_completion();
        },
    }
    Ok(())
}

/// Initialize logger with variable format and verbosity
/// Verbosity is decided based on `config` if no `-v` parameter was passed
/// otherwise it is overwritten.
/// Default level is "INFO"
fn init_logger(
    verbose: u8,
    format: &LoggerFormat,
    logger_level: &str,
    sentry: Option<SentryConfig>,
) -> Option<sentry::ClientInitGuard> {
    let verbosity = match verbose {
        0 => match logger_level {
            "debug" => Level::DEBUG,
            "trace" => Level::TRACE,
            "error" => Level::ERROR,
            "warn" => Level::WARN,
            _ => Level::INFO,
        },
        1 => Level::DEBUG,
        _ => Level::TRACE,
    };

    let tracing_layer = tracing_subscriber::fmt::Layer::new()
        .with_writer(std::io::stdout.with_max_level(verbosity));
    match format {
        LoggerFormat::Json => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry()
                    .with(tracing_layer.json())
                    .with(if sentry.is_some() {
                        Some(sentry::integrations::tracing::layer())
                    } else {
                        None
                    }),
            )
            .expect("Failed to set up global json subscriber");
        },
        LoggerFormat::Pretty => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry()
                    .with(tracing_layer.pretty())
                    .with(if sentry.is_some() {
                        Some(sentry::integrations::tracing::layer())
                    } else {
                        None
                    }),
            )
            .expect("Failed to set up global pretty subscriber");
        },
        LoggerFormat::Compact => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry()
                    .with(tracing_layer.compact())
                    .with(if sentry.is_some() {
                        Some(sentry::integrations::tracing::layer())
                    } else {
                        None
                    }),
            )
            .expect("Failed to set up global compact subscriber");
        },
        LoggerFormat::Full => {
            tracing::subscriber::set_global_default(
                tracing_subscriber::registry()
                    .with(tracing_layer)
                    .with(if sentry.is_some() {
                        Some(sentry::integrations::tracing::layer())
                    } else {
                        None
                    }),
            )
            .expect("Failed to set up global compact subscriber");
        },
    };
    let sentry_guard = sentry.map(|cfg| {
        sentry::init((
            cfg.dsn.as_str(),
            sentry::ClientOptions {
                release: sentry::release_name!(),
                environment: Some(Cow::from(cfg.environment)),
                // debug: true,
                traces_sample_rate: 1.0,
                // enable_profiling: true,
                // profiles_sample_rate: 1.0,
                ..Default::default()
            },
        ))
    });
    info!(?verbosity, ?format, "Initialized logger");
    sentry_guard
}
