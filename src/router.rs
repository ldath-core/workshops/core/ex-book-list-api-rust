use axum::{
    middleware,
    routing::{get, post},
    Extension, Router,
};
use tower_http::{
    cors::CorsLayer,
    trace::{DefaultOnRequest, DefaultOnResponse, TraceLayer},
};
use tracing::{info, Level};

use crate::{state::AppState, traits::DatabaseHandler};
use handlers::graphql::{build_schema, graphql_handler, graphql_ws_handler};
use handlers::{
    method_not_allowed, not_found,
    rest::{create_book, delete_book, get_book, get_books, get_health, update_book},
};
pub use models::{NewBook, NewImage, UpdateBook, UpdateImage};

mod handlers;
mod macros;
mod models;
#[cfg(test)]
mod tests;

/// Return router configured for the book API
pub fn get_router<D>(app_services: AppState<D>) -> Router
where
    D: DatabaseHandler + Clone + Send + Sync + 'static,
{
    let graphql_schema = build_schema(app_services.clone());
    let router: Router = Router::new()
        .route("/health", get(get_health::<D>))
        .route("/books", get(get_books::<D>).post(create_book::<D>))
        .route(
            "/books/:id",
            get(get_book::<D>)
                .put(update_book::<D>)
                .patch(update_book::<D>)
                .delete(delete_book::<D>),
        )
        .route("/graphql", post(graphql_handler::<D>))
        .route("/graphql/ws", get(graphql_ws_handler::<D>))
        .layer(middleware::from_fn(method_not_allowed))
        .layer(
            TraceLayer::new_for_http()
                .on_request(DefaultOnRequest::new().level(Level::INFO))
                .on_response(DefaultOnResponse::new().level(Level::INFO)),
        )
        .layer(Extension(graphql_schema))
        .with_state(app_services); // Somehow moving state to the front changes the type to Router<Db> and causes problems
    Router::new().fallback(not_found).nest("/v1", router)
}

/// Add CORS layer to ther router
///
/// # Examples
///
/// ```ignore
///  # let router = get_router(db::Db)
///  router = add_cors_layer(router.clone());
/// ```
pub fn add_cors_layer(r: Router) -> Router {
    r.layer(CorsLayer::permissive())
}

pub async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("Expect shutdown signal handler");
    info!("received shutdown signal");
}
