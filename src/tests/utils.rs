use axum::body::HttpBody;
use http::{Request, Uri};
use hyper::{Body, Response};
use rand::{self, Rng};
use serde_json::Value;
use sqlx::{query, PgPool};
use std::{
    net::{SocketAddr, TcpListener},
    sync::Arc,
    thread,
    time::Duration,
};
use tokio::{runtime::Runtime, sync::oneshot};

use super::{LISTEN_IP, REDIS_CONN_STR, REDIS_EXPIRY_SEC};
use crate::{
    db::{cache::Cache, Book, Db, Image, ImageType},
    event_bus::EventBus,
    router::get_router,
    state::AppState,
    traits::DatabaseConnector,
};

/// Retries the request continously for n seconds
pub async fn retry_request(uri: Uri, n: u32) -> Option<Response<Body>> {
    for _ in 0..n * 2 {
        let response = hyper::Client::new().get(uri.clone()).await;
        if let Ok(resp) = response {
            return Some(resp);
        }
        tokio::time::sleep(std::time::Duration::from_millis(500)).await;
    }
    None
}

/// Starts the server in separate thread and waits until it makes a simplest response.
/// The thread is automatically terminated when returned sender goes out of scope.
pub async fn start_background_server(
    listener: TcpListener,
    pool: PgPool,
    cache: Cache,
) -> oneshot::Sender<()> {
    let addr = listener.local_addr().unwrap();
    let event_bus = Arc::new(EventBus::new());
    let db = Db::new(pool, cache, event_bus.clone()).await.unwrap();
    db.migrate_db().await.unwrap();

    let app_services = AppState::new(event_bus, db);
    let router = get_router(app_services);
    // Starting our app in a background thread, rx automatically closes it when we go out of scope
    let (tx, rx) = oneshot::channel::<()>();
    thread::spawn(move || {
        Runtime::new().unwrap().block_on(async move {
            let server = axum::Server::from_tcp(listener)
                .unwrap()
                .serve(router.into_make_service());
            tokio::select! {
                _ = rx => {},
                _ = server => {},
            }
        });
    });
    // wait for the server to start responding
    retry_request(uri(addr, "/v1/health"), 5).await.unwrap();

    tx
}

pub async fn body_as_string<T>(r: hyper::Response<T>) -> String
where
    T: HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();

    String::from_utf8(hyper::body::to_bytes(into).await.unwrap().to_vec()).unwrap()
}

pub async fn body_as_json<T>(r: hyper::Response<T>) -> Value
where
    T: HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();
    let body: Value = serde_json::from_slice(&hyper::body::to_bytes(into).await.unwrap()).unwrap();
    body
}

pub fn uri(addr: SocketAddr, path_and_query: &str) -> Uri {
    let mut parts = http::uri::Parts::default();
    parts.scheme = Some("http".parse().unwrap());
    parts.authority = Some(addr.to_string().parse().unwrap());
    parts.path_and_query = Some(path_and_query.parse().unwrap());
    Uri::from_parts(parts).unwrap()
}

/// Starts a listener on a random port
/// Returns listener instance and corresponding socket address
pub fn get_random_port_listener(ip: &str) -> (TcpListener, SocketAddr) {
    let listener = TcpListener::bind((ip, 0)).unwrap();
    let addr = listener.local_addr().unwrap();
    (listener, addr)
}

pub async fn get_temporary_db_pool(db_name: &str) -> PgPool {
    sqlx::PgPool::connect(&format!(
        "postgres://book-list:2vQU45haQCnDS8sO@localhost:5432/{db_name}"
    ))
    .await
    .unwrap()
}

pub async fn create_random_database(pool: &PgPool, db_template: &str) -> String {
    const CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz";
    const DB_NAME_LEN: usize = 30;
    let mut rng = rand::thread_rng();
    let new_database: String = (0..DB_NAME_LEN)
        .map(|_| {
            let idx = rng.gen_range(0..CHARSET.len());
            CHARSET[idx] as char
        })
        .collect();

    query(&format!(
        "CREATE DATABASE {new_database} TEMPLATE {db_template}"
    ))
    .execute(pool)
    .await
    .unwrap();
    new_database
}

pub async fn drop_database(name: &str, conn_str: &str) {
    let pool = sqlx::PgPool::connect(conn_str).await.unwrap();
    query(&format!("DROP DATABASE {name}"))
        .execute(&pool)
        .await
        .unwrap();
    pool.close().await;
}

pub async fn drop_database_forced(name: &str, conn_str: &str) {
    let pool = sqlx::PgPool::connect(conn_str).await.unwrap();
    query(&format!("DROP DATABASE {name} WITH (FORCE)"))
        .execute(&pool)
        .await
        .unwrap();
    pool.close().await;
}

pub fn get_vec_books() -> Vec<Book> {
    vec![
        Book {
            id: -1,
            title: "book1".to_string(),
            author: "author1".to_string(),
            year: "2137".to_string(),
            description: None,
            image: None,
        },
        Book {
            id: -1,
            title: "book2".to_string(),
            author: "author2".to_string(),
            year: "2138".to_string(),
            description: Some("descripton only".to_string()),
            image: None,
        },
        Book {
            id: -1,
            title: "book3".to_string(),
            author: "author3".to_string(),
            year: "2139".to_string(),
            description: None,
            image: Some(Image {
                type_: ImageType::Url,
                data: "url.com/onlyurl.jpg".to_string(),
            }),
        },
        Book {
            id: -1,
            title: "book4".to_string(),
            author: "author4".to_string(),
            year: "2140".to_string(),
            description: Some("descripton".to_string()),
            image: Some(Image {
                type_: ImageType::Base64,
                data: "==onlybase64".to_string(),
            }),
        },
        Book {
            id: -1,
            title: "book5".to_string(),
            author: "author5".to_string(),
            year: "2141".to_string(),
            description: None,
            image: Some(Image {
                type_: ImageType::Base64,
                data: "==onlybase64second".to_string(),
            }),
        },
    ]
}

pub async fn send_req(req: Request<Body>) -> Response<Body> {
    let client = hyper::Client::new();
    client.request(req).await.unwrap()
}

pub async fn prepare_test_dependencies(db_name: &str) -> (PgPool, TcpListener, SocketAddr, Cache) {
    let pool = get_temporary_db_pool(db_name).await;
    let cache = Cache::new(
        Cache::connect_redis(REDIS_CONN_STR).await.unwrap(),
        Duration::from_secs(REDIS_EXPIRY_SEC),
    )
    .await;
    let (listener, addr) = get_random_port_listener(LISTEN_IP);
    (pool, listener, addr, cache)
}
