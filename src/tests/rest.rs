//! tests for rest endpoints
//!

use http::{HeaderValue, Request, StatusCode, Uri};
use hyper::Body;
use rand::distributions::{Alphanumeric, DistString};
use serde_json::json;
use std::time::Duration;

use super::{
    utils::{
        body_as_json, body_as_string, get_vec_books, prepare_test_dependencies, retry_request,
        send_req, start_background_server, uri,
    },
    TestContext,
};

#[ctor::ctor]
async fn setup() {
    super::suite_setup();
}

#[tokio::test(flavor = "multi_thread")]
async fn test_get_health() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;

    let resp = retry_request(uri(addr, "/v1/health"), 10).await.unwrap();
    assert_eq!(resp.status(), StatusCode::OK);
    assert_eq!(
        resp.headers().get("content-type").unwrap(),
        &HeaderValue::from_static("application/json")
    );

    let body = body_as_string(resp).await;
    assert!(body.contains("\"alive\":true"));
}

#[tokio::test(flavor = "multi_thread")]
async fn test_add_many_update_delete_get_all() {
    let mut books = get_vec_books();
    assert!(books.len() >= 2);
    let updated_book = json!({
        "title": "updatedtitle",
        "author": "updatedauthor",
        "year": "1999",
        "description": "updated description",
        "image": {"type": "base64", "data": "=base64"}
    });

    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;

    // Add many books do the database in successive requests
    for b in &mut books {
        let resp = send_req(
            Request::post(uri(addr, "/v1/books"))
                .header("content-type", "application/json")
                .body(Body::from(serde_json::to_vec(&b).unwrap()))
                .unwrap(),
        )
        .await;
        assert_eq!(resp.status(), StatusCode::CREATED);
        b.id = i32::try_from(body_as_json(resp).await["content"]["id"].as_i64().unwrap()).unwrap();
    }
    // get all books to verify previous step added them correctly
    let resp = send_req(get(uri(addr, "/v1/books"))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(
        serde_json::to_value(&books).unwrap(),
        body["content"]["results"]
    );

    // update one of the books
    let update_id = books[0].id;

    let resp = send_req(
        Request::put(uri(addr, &format!("/v1/books/{update_id}")))
            .header("content-type", "application/json")
            .body(Body::from(serde_json::to_vec(&updated_book).unwrap()))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::ACCEPTED);
    // get the book that was updated in the previous step and verify changes
    let resp = send_req(get(uri(addr, &format!("/v1/books/{update_id}")))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    let queried = body["content"].as_object().unwrap();
    for (k, v) in updated_book.as_object().unwrap().iter() {
        assert_eq!(v, &queried[k]); // all the keys in queried book are equal to updated
    }

    // take the last book created and delete it
    let delete_id = books.last().unwrap().id;

    let resp = send_req(del(uri(addr, &format!("/v1/books/{delete_id}")))).await;
    assert_eq!(resp.status(), StatusCode::ACCEPTED);
    // send request for the book deleted in previous step
    let resp = send_req(get(uri(addr, &format!("/v1/books/{delete_id}")))).await;
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);

    // get all books to verify there is one less after sleep to let the cache keys expire
    tokio::time::sleep(Duration::from_secs(5)).await;
    let resp = send_req(get(uri(addr, "/v1/books"))).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(
        body["content"]["results"].as_array().unwrap().len(),
        books.len() - 1
    );
}

#[tokio::test(flavor = "multi_thread")]
async fn test_create_1000_books_get_with_pagination() {
    let n: usize = 1000;
    let skip = 10;
    let limit = 100;
    assert!(skip + limit < n);

    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;

    // Add {n} books do the database in successive requests
    for _ in 0..n {
        let resp = send_req(
            Request::post(uri(addr, "/v1/books"))
                .header("content-type", "application/json")
                .body(Body::from(rand_book()))
                .unwrap(),
        )
        .await;
        assert_eq!(resp.status(), StatusCode::CREATED);
    }
    // get {limit} number of books from the total of {n}
    let resp = send_req(get(uri(
        addr,
        &format!("/v1/books?skip={skip}&limit={limit}"),
    )))
    .await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(body["content"]["count"], n);
    assert_eq!(body["content"]["skip"], skip);
    assert_eq!(body["content"]["limit"], limit);
    assert_eq!(body["content"]["results"].as_array().unwrap().len(), limit);
}

fn rand_str(n: usize) -> String {
    Alphanumeric.sample_string(&mut rand::thread_rng(), n)
}

fn rand_book() -> Vec<u8> {
    serde_json::to_vec(&json!({
        "title": rand_str(50),
        "author": rand_str(30),
        "year": rand_str(4),
        "description": rand_str(200),
    }))
    .unwrap()
}

fn get(uri: Uri) -> Request<Body> {
    Request::get(uri).body(Body::empty()).unwrap()
}

fn del(uri: Uri) -> Request<Body> {
    Request::delete(uri).body(Body::empty()).unwrap()
}
