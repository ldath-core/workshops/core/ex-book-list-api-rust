//! tests for graphql endpoints
use async_graphql::futures_util::{SinkExt, StreamExt};
use futures::{
    stream::{SplitSink, SplitStream},
    Stream,
};
use http::{
    header::{
        CONNECTION, HOST, SEC_WEBSOCKET_KEY, SEC_WEBSOCKET_PROTOCOL, SEC_WEBSOCKET_VERSION, UPGRADE,
    },
    HeaderValue, Request, StatusCode, Uri,
};
use hyper::Body;

use serde_json::Value;

use std::{
    net::{IpAddr, SocketAddr},
    str::FromStr,
    time::Duration,
};
use tokio::{net::TcpStream, time::timeout};
use tokio_tungstenite::{connect_async, tungstenite, MaybeTlsStream, WebSocketStream};

use super::{
    utils::{body_as_json, prepare_test_dependencies, send_req, start_background_server},
    TestContext,
};

const GRAPHQL_ENDPOINT: &str = "v1/graphql";
const GRAPHQL_WS_ENDPOINT: &str = "v1/graphql/ws";

#[ctor::ctor]
async fn setup() {
    super::suite_setup();
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_get_health() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    let health_query = r#"{"query": "query { health { alive db }}"}"#;
    let resp = send_req(Request::post(uri).body(Body::from(health_query)).unwrap()).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert!(body["data"]["health"]["alive"].as_bool().unwrap());
    assert!(body["data"]["health"]["db"].as_bool().unwrap());
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_create_book() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    let s = graphql_create_query();
    let resp = send_req(Request::post(uri).body(Body::from(s)).unwrap()).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!(
        "title",
        body["data"]["createBook"]["title"].as_str().unwrap()
    );
    assert_eq!(
        "author",
        body["data"]["createBook"]["author"].as_str().unwrap()
    );
    assert_eq!("2137", body["data"]["createBook"]["year"].as_str().unwrap());
    assert!(body["data"]["createBook"]["image"].as_null().is_none()); // verify that the image field is some
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_delete_book() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    let delete_query = r#"{"query": "mutation delete { deleteBook (id: 1) }"}"#;
    let resp = send_req(Request::post(uri).body(Body::from(delete_query)).unwrap()).await;
    assert_eq!(resp.status(), StatusCode::OK);
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_create_then_get_books() {
    const N_CREATED: usize = 5;
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    let create_query = graphql_create_query();
    for _ in 0..N_CREATED {
        let resp = send_req(
            Request::post(&uri)
                .body(Body::from(create_query.clone()))
                .unwrap(),
        )
        .await;
        assert_eq!(resp.status(), StatusCode::OK);
    }

    let get_books_query = "\
{\"query\": \"query {\
    books(skip: 0, limit: 20) {\
        id \
        title \
        author \
        year \
        description \
        image { \
            type \
            data \
        }\
    }\
}\"\
}";
    let resp = send_req(
        Request::post(&uri)
            .body(Body::from(get_books_query))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::OK);
    let body = body_as_json(resp).await;
    let books = body["data"]["books"].as_array().unwrap();
    assert_eq!(books.len(), N_CREATED);
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_create_then_get_book() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    let create_query = graphql_create_query();
    let resp = send_req(Request::post(&uri).body(Body::from(create_query)).unwrap()).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    let created_id: i64 = body["data"]["createBook"]["id"]
        .as_str()
        .unwrap()
        .parse()
        .unwrap();

    let get_book_query = format!(
        "\
{{\"query\": \"query {{\
    book(id: {created_id}) {{\
        id \
        title \
        author \
        year \
        description \
        image {{ \
            type \
            data \
        }}\
    }}\
}}\"\
}}"
    );
    let resp = send_req(
        Request::post(&uri)
            .body(Body::from(get_book_query))
            .unwrap(),
    )
    .await;
    assert_eq!(resp.status(), StatusCode::OK);

    let body = body_as_json(resp).await;
    assert_eq!("title", body["data"]["book"]["title"].as_str().unwrap());
    assert_eq!(
        created_id,
        body["data"]["book"]["id"]
            .as_str()
            .unwrap()
            .parse::<i64>()
            .unwrap()
    );
}

#[tokio::test(flavor = "multi_thread")]
async fn graphql_subscription() {
    let ctx = TestContext::new().await;
    let (pool, listener, addr, cache) = prepare_test_dependencies(&ctx.db_name).await;
    let _tx = start_background_server(listener, pool, cache).await;
    let uri = graphql_uri(&addr.ip(), &addr.port());

    // start subscription websocket connection
    let subscription_query = r#"{
"id": "1",
"type": "start",
"payload": {
        "query": "subscription eventbus {
        bookEvents {
            eventType
            id
            book {
                id
                title
                author
                description
                image {
                    type
                    data
                }
            }
        }
        }"
    }
}"#
    .replace('\n', " ");
    let (_, mut read) = initialize_graphql_subscription(subscription_query, &addr).await;

    // send create/delete/update events
    let c = graphql_create_query();
    send_req(Request::post(&uri).body(Body::from(c.clone())).unwrap()).await;
    send_req(Request::post(&uri).body(Body::from(c)).unwrap()).await;
    let u = graphql_update_query();
    send_req(Request::post(&uri).body(Body::from(u)).unwrap()).await;
    let d = graphql_delete_query();
    send_req(Request::post(&uri).body(Body::from(d)).unwrap()).await;

    // check if they are visible in subscription
    let x = get_subscription_response(&mut read).await;
    assert_eq!(
        "CREATED",
        x["payload"]["data"]["bookEvents"]["eventType"]
            .as_str()
            .unwrap()
    );
    assert_eq!(
        "author",
        x["payload"]["data"]["bookEvents"]["book"]["author"]
            .as_str()
            .unwrap()
    );

    let x = get_subscription_response(&mut read).await;
    assert_eq!(
        "CREATED",
        x["payload"]["data"]["bookEvents"]["eventType"]
            .as_str()
            .unwrap()
    );
    assert_eq!(
        "title",
        x["payload"]["data"]["bookEvents"]["book"]["title"]
            .as_str()
            .unwrap()
    );

    let x = get_subscription_response(&mut read).await;
    assert_eq!(
        "UPDATED",
        x["payload"]["data"]["bookEvents"]["eventType"]
            .as_str()
            .unwrap()
    );
    assert_eq!(
        "updated-title",
        x["payload"]["data"]["bookEvents"]["book"]["title"]
            .as_str()
            .unwrap()
    );

    let x = get_subscription_response(&mut read).await;
    assert_eq!(
        "DELETED",
        x["payload"]["data"]["bookEvents"]["eventType"]
            .as_str()
            .unwrap()
    );
    assert_eq!(
        1,
        x["payload"]["data"]["bookEvents"]["id"].as_i64().unwrap()
    );
    assert!(x["payload"]["data"]["bookEvents"]["book"]
        .as_null()
        .is_some());
}

async fn initialize_graphql_subscription(
    query: String,
    addr: &SocketAddr,
) -> (
    SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, tungstenite::Message>,
    SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>,
) {
    let ws_uri = graphql_ws_uri(&addr.ip(), &addr.port());
    let mut req = tungstenite::http::Request::get(ws_uri).body(()).unwrap();
    req.headers_mut().insert(
        SEC_WEBSOCKET_PROTOCOL,
        HeaderValue::from_str("graphql-transport-ws").unwrap(),
    );
    req.headers_mut().insert(
        SEC_WEBSOCKET_KEY,
        HeaderValue::from_str("KlaybDlnS2U4Q/nWMSeguA==").unwrap(),
    );
    req.headers_mut()
        .insert(SEC_WEBSOCKET_VERSION, HeaderValue::from_str("13").unwrap());
    req.headers_mut().insert(
        HOST,
        HeaderValue::from_str(&format!("{}:{}", &addr.ip(), &addr.port())).unwrap(),
    );
    req.headers_mut()
        .insert(CONNECTION, HeaderValue::from_str("upgrade").unwrap());
    req.headers_mut()
        .insert(UPGRADE, HeaderValue::from_str("websocket").unwrap());
    let (ws_stream, _response) = connect_async(req).await.unwrap();
    let (mut write, mut read) = ws_stream.split();

    let connection_init = r#"{
        "type": "connection_init"
    }"#;
    write
        .send(tungstenite::Message::Text(connection_init.to_string()))
        .await
        .unwrap();

    write.send(tungstenite::Message::Text(query)).await.unwrap();
    read.next().await.unwrap().unwrap();

    (write, read)
}

async fn get_subscription_response<S>(read: &mut S) -> Value
where
    S: Stream<Item = Result<tungstenite::Message, tungstenite::Error>> + Unpin,
{
    let x = timeout(Duration::from_secs(5), read.next())
        .await
        .unwrap()
        .unwrap()
        .unwrap();
    let x: Value = serde_json::from_str(x.to_text().unwrap()).unwrap();
    x
}

fn graphql_create_query() -> String {
    String::from(
        r#"{"query": "mutation {
        createBook(book: {
            title: \"title\" 
            author: \"author\" 
            year: \"2137\" 
            description: \"description\" 
            image: { 
                type: BASE_64 
                data: \"==base64\" 
            } 
        }) {
            id 
            title 
            author 
            year 
            description 
            image { 
                type 
                data 
            }
        }
    }"
}"#,
    )
    .replace('\n', " ")
}

fn graphql_update_query() -> String {
    String::from(
        r#"{"query": "mutation {
        updateBook(id: 1 book: {
            title: \"updated-title\" 
            author: \"updated-author\" 
            year: \"2138\" 
            description: \"updated-description\" 
            image: { 
                type: BASE_64 
                data: \"==updatedbase64\" 
            } 
        }) {
            id 
            title 
            author 
            year 
            description 
            image { 
                type 
                data 
            }
        }
    }"
}"#,
    )
    .replace('\n', " ")
}

fn graphql_delete_query() -> String {
    String::from("{\"query\": \"mutation delete { deleteBook (id: 1) }\"}")
}

fn graphql_uri(ip: &IpAddr, port: &u16) -> Uri {
    Uri::from_str(&format!("http://{ip}:{port}/{GRAPHQL_ENDPOINT}")).unwrap()
}

fn graphql_ws_uri(ip: &IpAddr, port: &u16) -> Uri {
    Uri::from_str(&format!("ws://{ip}:{port}/{GRAPHQL_WS_ENDPOINT}")).unwrap()
}
