FROM rust:1.67

WORKDIR /var/app
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
COPY migrations /var/app/migrations
RUN cargo build --release && cp ./target/release/book-list /bin/book-list

EXPOSE 8080

ENTRYPOINT ["/bin/book-list"]
CMD ["--config", "/secrets/local.env.yaml", "-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
